<!--sharepost modal-->
<div id="sharepostmodal" class="modal sharepost_modal post-popup main_modal custom_modal">
   <div class="hidden_header">
      <div class="content_header">
         <button class="close_span cancel_poup waves-effect">
         <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
         </button>
         <p class="modal_header_xs">Share post</p>
         <a type="button" class="post_btn action_btn active_post_btn post_btn_xs sharebtn close_modal">Share</a>
      </div>
   </div>
   <div class="modal-content">
      <div class="new-post active">
         <div class="top-stuff">
            <!--<div class="side-user">-->
            <div class="postuser-info">
               <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
               <div class="desc-holder">
                  <p class="profile_name share_profile_name">User name</p>
                  <label id="share_tag_person"></label>
                  <div class="public_dropdown_container">
                     <a class="dropdown_text dropdown-button-left sharepostcreateprivacylabel" onclick="privacymodal(this)" href="javascript:void(0)" data-modeltag="sharepostcreateprivacylabel" data-fetch="no" data-label="sharepost">
                     <span> Public </span>
                     <i class="zmdi zmdi-caret-down"></i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="settings-icon comment_setting_icon">
               <a class="dropdown-button " href="javascript:void(0)" data-activates="share_dropdown">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="share_dropdown" class="dropdown-content custom_dropdown">
                  <li>
                     <a href="javascript:void(0)">
                     <input type="checkbox" id="toolbox_disable_sharing" />
                     <label for="toolbox_disable_sharing">Disable Sharing</label>
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0)" class="savepost-link">
                     <input type="checkbox" id="toolbox_disable_comments" />
                     <label for="toolbox_disable_comments">Disable Comments</label>
                     </a>
                  </li>
               </ul>
            </div>
            <div class="share_dropdown_container">
               <a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="share_post_dropdown">
               <span>
               Share on your wall
               </span>
               <i class="zmdi zmdi-caret-down"></i>
               </a>
               <ul id="share_post_dropdown" class="dropdown-content custom_dropdown share_post_dropdown share_post_new">
                  <li><a href="javascript:void(0)" class="share-privacy">Share on your wall</a></li>
                  <li><a href="javascript:void(0)" id="share_friends" class="share-to-friends share-privacy">Share on a friend's wall</a></li>
                  <li><a href="javascript:void(0)" id="share_Group" class="share-to-groupwall share-privacy">Share on group wall</a></li>
                  <li><a href="javascript:void(0)" id="share_message" class="share-as-message share-privacy">Share as message</a></li>
                  <li><a href="javascript:void(0)" class="share-privacy">Share on Facebook</a></li>
               </ul>
            </div>
         </div>
         <div class="clear"></div>
         <div class="scroll_div">
            <div class="npost-content">
               <div class="share-friends">
                  <span class="title">Share with :</span>
                  <div class="input-holder">
                     <a href="javascript:void(0)" class="compose_addpersonAction">Adel Hasanat</a><span> and </span><a href="javascript:void(0)" class="compose_addpersonAction">2 Others</a>
                  </div>
               </div>
               <div class="share-message">
                  <span class="title">Receipent:</span>
                  <div class="input-holder">
                     <div class="sliding-middle-out anim-area underlined">
                        <select class="userselect2" tabindex="-1"  id="frndid"></select>
                     </div>
                  </div>
               </div>
               <div class="share-groupwall">
                  <span class="title">Groups:</span>
                  <div class="input-holder">
                     <div class="sliding-middle-out anim-area underlined">
                        <select class="select2" multiple="" tabindex="-1" >
                           <option>group-1</option>
                           <option>group-2</option>
                           <option>group-3</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="post-mcontent">
                  <div class="desc post_comment_box">
                     <textarea id="share_post_comment" placeholder="Say something about this..." class="materialize-textarea comment_textarea"></textarea>
                  </div>
                  <div class="org-post mt-0">
                     <div class="post-list">
                        <div class="post-holder">
                           <!-- shared post -->
                           <div class="post-content share-feedpage">
                              <div class="post-img-holder">
                                 <div class="lgt-gallery post-img two-img">
                                    <a href="images/post-img2.jpg" data-size="1600x1600" data-med="images/post-img2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                    <img class="himg" src="images/post-img2.jpg" alt="" />
                                    </a>
                                    <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                                    <img class="vimg" src="images/post-img3.jpg" alt="" />
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="sharepost-info">
                              <p><a href="javascript:void(0)">Nimish Parekh</a> with <a href="javascript:void(0)">Markand Trivedi</a> and <a href="javascript:void(0)">3 others</a> at <span class="sharepost-location">Ahmedabad</span></p>
                              <span class="timestamp">4 Apr 2011<span class="glyphicon glyphicon-globe"></span></span>										
                           </div>
                           <!-- end shared post -->
                        </div>
                     </div>
                     <div class="show-fullpost-holder">
                        <a href="javascript:void(0)" class="show-fullpost">Show All <span class="glyphicon glyphicon-arrow-down"></span></a>
                     </div>
                  </div>
                  <div class="location_parent">
                     <label id="share_selectedlocation" class="share_selected_loc"></label>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal-footer">
      <div class="new-post active">
         <div class="post-bcontent">
            <div class="footer_icon_container">
               <button class="comment_footer_icon waves-effect" id="compose_addpersonAction">
               <i class="zmdi zmdi-account"></i>
               </button>
               <button class="comment_footer_icon waves-effect" data-query="all" onfocus="filderMapLocationModal(this)">
               <i class="zmdi zmdi-pin"></i>
               </button>
            </div>
            <div class="public_dropdown_container_xs">
               <a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="post_privacy_share_xs">
               <span>
               Public
               </span>
               <i class="zmdi zmdi-caret-down"></i>
               </a>
               <ul id="post_privacy_share_xs" class="dropdown-privacy dropdown-content public_dropdown_xs">
                  <li>
                     <a href="javascript:void(0)">
                        <!--<i class="zmdi zmdi-case"></i>-->Private
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0)">
                        <!--<i class="zmdi zmdi-account"></i>-->Friends
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0)">
                        <!--<i class="zmdi zmdi-globe"></i>-->Public
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0)" onclick="addpersonmodal()">
                        <!--<i class="zmdi zmdi-network-locked"></i>-->Custom
                     </a>
                  </li>
               </ul>
            </div>
            <div class="post-bholder">
               <div class="post-loader"><img src="images/home-loader.gif"/></div>
               <div class="hidden_xs">
                  <a class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
                  <a class="btngen-center-align waves-effect" onclick="verify()">Share</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>