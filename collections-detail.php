<?php include("header.php"); ?>	
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
         <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container fulltab">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="main-content with-lmenu general-page generaldetails-page collections-page collectiondetails-page main-page">
         <div class="combined-column">
            <div class="content-box">
               <div class="cbox-title nborder">
                  <i class="mdi mdi-folder-open"></i>
                  Collections
                  <a href="collections.php" class="backbtn"><i class="mdi mdi-menu-left"></i> Back to collections</a>
               </div>
               <div class="cbox-desc">
                  <div class="tab-content view-holder">
                     <div class="general-details">
                        <div class="gdetails-summery">
                           <div class="main-info">
                              <div class="imgholder">
                                 <img src="images/additem-collections.png"/>
                                 <div class="back-link">
                                    <a href="collections.php" class="waves-effect waves-theme"><i class="mdi mdi-arrow-left"></i></a>
                                 </div>
                                 <div class="action-links item_detail_dropdown">
                                    <a href="javascript:void(0)" class="orglink share-it waves-effect waves-theme">
                                    <i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
                                    </a>
                                    <div class="settings-icon">
                                       <a class="dropdown-button waves-effect waves-theme" href="javascript:void(0)" data-activates="detail_setting">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="detail_setting" class="dropdown-content custom_dropdown">
                                          <li>
                                             <a href="javascript:void(0)" onclick="EditCollection()">Edit Collection</a>
                                          </li>
                                          <li>
                                             <a>Manage Members</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="CollectionFollowers()">Collection followers</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="PreferenceModel()">Preferences</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="DeleteCollection()">Delete Collection</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="generateDiscard('dis_unfollow')">Unfollow Collection</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="reportAbuseModal()">Report abuse</a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="content-holder dateholder">
                                 <div class="userinfo">
                                    <img src="images/chat-1.png">
                                 </div>
                              </div>
                              <div class="content-holder gdetails-moreinfo expandable-holder">
                                 <a href="javascript:void(0)" class="expandable-link invertsign" onclick="mng_expandable(this)"><i class="mdi mdi-chevron-down"></i></a>
                                 <div class="expandable-area">
                                    <div class="username">
                                       <span>Brain Koberlein</span>
                                    </div>
                                    <h4>Our Ireland</h4>
                                    <div class="icon-line">
                                       <span>1,840 followers</span>
                                       <span>-</span>
                                       <span>452 posts</span>
                                       <span>-</span>
                                       <span>public</span>
                                    </div>
                                    <div class="action-btns">
                                       <a class="noClick" onclick="collectionFollow(event,'collection1',this)">Follow</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="sideboxes">
                              <?php include('common/recently_joined.php'); ?>
                              <div class="content-box bshadow">
                                 <div class="cbox-desc">
                                    <div class="side-travad brand-ad">
                                       <div class="travad-maintitle">Best coffee in the world!</div>
                                       <div class="imgholder">
                                          <img src="images/brand-p.jpg">
                                       </div>
                                       <div class="descholder">
                                          <div class="ad-subtitle">We just get new starbucks coffee that is double in caffine that everybody is calling it a boost!</div>
                                          <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Explore</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="content-box bshadow">
                                 <div class="side-travad action-travad">
                                    <div class="travad-maintitle">
                                       <span class="iholder"><i class=”mdi mdi-account-group”></i></i></span>
                                       <h6>Heal Well</h6>
                                       <span class="adtext">Sponsored</span>
                                    </div>
                                    <div class="imgholder">
                                       <img src="images/groupad-actionvideo.jpg"/>
                                    </div>
                                    <div class="descholder">
                                       <div class="ad-title">Medical Research Methodolgy</div>
                                       <div class="ad-subtitle">Checkout the new video on our website exploring the latest techniques of medicine research</div>
                                       <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Learn More</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="post-column">
                           <?php include('common/new_post.php'); ?> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>
         <div class="new-post-mobile clear">
            <a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<?php  include('common/preference_popup.php'); ?>
<!--collection followers modal -->
<div id="collection_followers" class="modal item_members">
   <div class="modal_header">
      <button class="close_btn custom_modal_close_btn close_modal waves-effect">
      <i class="mdi mdi-close mdi-20px"></i>
      </button>
      <h3>Collection followers</h3>
   </div>
   <div class="custom_modal_content modal_content">
      <div class="follower-container">
         <ul>
            <li>
               <div class="follwer_profile">
                  <img class="circle" src="images/demo-profile.jpg" />
               </div>
               <div class="follwer_name">
                  <span>User Name</span>
               </div>
            </li>
            <li>
               <div class="follwer_profile">
                  <img class="circle" src="images/demo-profile.jpg" />
               </div>
               <div class="follwer_name">
                  <span>User Name</span>
               </div>
            </li>
            <li>
               <div class="follwer_profile">
                  <img class="circle" src="images/demo-profile.jpg" />
               </div>
               <div class="follwer_name">
                  <span>User Name</span>
               </div>
            </li>
            <li>
               <div class="follwer_profile">
                  <img class="circle" src="images/demo-profile.jpg" />
               </div>
               <div class="follwer_name">
                  <span>User Name</span>
               </div>
            </li>
            <li>
               <div class="follwer_profile">
                  <img class="circle" src="images/demo-profile.jpg" />
               </div>
               <div class="follwer_name">
                  <span>User Name</span>
               </div>
            </li>
         </ul>
      </div>
   </div>
</div>
<!--map modal-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include('common/uploadphoto_popup.php'); ?>
<?php include('common/addperson_popup.php'); ?>
<?php include('common/reportpost-popup.php'); ?>
<?php include('common/compose_post_popup.php'); ?>
<?php include('common/comment_popup.php'); ?>
<?php include('common/postopen_popup.php'); ?>
<?php  include('common/share_popup.php'); ?>
<?php include('common/editpost_popup.php'); ?>

<!--add collection modal-->
<div id="add_collection_modal" class="modal add-item-popup custom_md_modal collec_main_cus add_collection_new">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>			
            <h3>Edit collection</h3>
            <a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="main-pcontent">
            <form class="add-item-form">
               <div class="frow frowfull">
                  <div class="crop-holder" id="image-cropper">
                     <div class="cropit-preview"></div>
                     <div class="main-img">
                        <img src="images/additem-collections.png" class="ui-corner-all"/>
                     </div>
                     <div class="main-img1">
                        <img id="imageid" draggable="false"/>
                     </div>
                     <div class="btnupload custom_up_load" id="upload_img_action">
                        <div class="fileUpload">
                           <i class="zmdi zmdi-camera zmdi-hc-lg"></i>
                           <input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
                        </div>
                     </div>
                     <a  href="javascript:void(0)" class="btn btn-save image_save_btn image_save dis-none">
                     <span class="zmdi zmdi-check"></span>
                     </a>
                     <a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash">
                     <i class="mdi mdi-close mdi-20px"></i>
                     </a>
                  </div>
               </div>
               <div class="sidepad">
                  <div class="frow">
                     <input id="Collection_title" type="text" class="validate Collection_title" placeholder="Collection title" />
                  </div>
                  <div class="frow">
                     <textarea id="Collection_tagline" class="materialize-textarea mb0 md_textarea md_textarea_height" placeholder="Collection tagline"></textarea>
                     <span class="char-limit">0/80</span>
                  </div>
                  <div class="frow security-area">
                     <label>Visible To:</label>
                     <div class="right">
                        <a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="collection_privacy">
                        <span>
                        Public
                        </span>
                        <i class="mdi mdi-menu-down"></i>
                        </a>
                        <ul id="collection_privacy" class="dropdown-privacy dropdown-privacy dropdown-content new_drop_colle">
                           <li>
                              <a href="javascript:void(0)">
                              Private
                              </a>
                           </li>
                           <li>
                              <a href="javascript:void(0)">
                              Friends
                              </a>
                           </li>
                           <li>
                              <a href="javascript:void(0)">
                              Public
                              </a>
                           </li>
                           <li>
                              <a href="javascript:void(0)" onclick="addpersonmodal()">
                              Custom
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align  close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Create</a>
   </div>
</div>
<?php include('common/discard_popup.php'); ?>

<?php include("script.php"); ?>	
</body>
</html>