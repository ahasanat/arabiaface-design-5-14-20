<div class="float-chat anim-side">
   <div class="chat-button float-icon"><span class="icon-holder">icon</span></div>
   <div class="chat-section">
      <a href="javascript:void(0)" class="close-chat"><i class="mdi mdi-close mdi-20px"></i>`</a>
      <div class="loading-holder">
         <div class="chat-tabs">
            <div class="chat-controls actions">
               <div class="chat-search">
                  <input type="text" class="chatwallsearch" placeholder="search a keyword">              
                  <div class="btn-holder">
                     <a href="javascript:void()"><i class="zmdi zmdi-search"></i></a>
                     <a onclick="openChatSearch(this,'close')" href="javascript:void()"><i class="mdi mdi-close"></i></a>
                  </div>
               </div>
               <div class="dropdown dropdown-custom">
                  <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="search_online">
                     <i class="zmdi zmdi-more"></i>
                  </a>
                  <ul id="search_online" class="dropdown-content custom_dropdown search_online_new">
                     <li>
                        <ul  class="tabs">
                           <li class="tab active"><a href="#chat-friends" data-toggle="tab" aria-expanded="false" onclick="openChatSearch(this,'close'),callBuddies();">Buddies</a></li>
                           <li class="tab"><a href="#chat-online" data-toggle="tab" aria-expanded="true" onclick="openChatSearch(this,'close'),callOnlineUsers()">Online</a></li>
                           <li class="tab callRecentMessagesUsers"><a href="#chat-recent" data-toggle="tab" aria-expanded="false" onclick="openChatSearch(this,'close');">Recent</a></li>
                        </ul>
                     </li>
                     <li><a href="javascript:void(0)" data-toggle="tab" aria-expanded="false" onclick="openChatSearch(this,'open')" href="javascript:void(0)">Search</a></li>
                  </ul>    
               </div>
            </div>
            <div class="tab-content">
               <div class="tab-pane fade friendschat-pane active in" id="chat-friends">
                  <span class="ctitle">Friends</span>
                  <div class="nice-scroll recentchat-scroll">
                     <ul class="chat-away chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user4" class="atag">
                              <span class="img-holder"><img src="images/chat-4.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Elina Grey</span>
                              <span class="info">Cook</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-online chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user1" class="atag active">
                              <span class="img-holder"><img src="images/chat-1.png" /><span class="badge">3</span></span>
                              <span class="desc-holder">
                              <span class="uname">Oliver Rogers</span>
                              <span class="info">Graphics Designer</span>                       
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-away chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user5" class="atag">
                              <span class="img-holder"><img src="images/chat-5.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Salma Musa</span>
                              <span class="info">Photographer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-offline chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user8" class="atag">
                              <span class="img-holder"><img src="images/chat-7.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Kelly Mark</span>
                              <span class="info">Hair Stylist</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-offline chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user9" class="atag">
                              <span class="img-holder"><img src="images/chat-8.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Jason Gomez</span>
                              <span class="info">CEO</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-online chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user3" class="atag">
                              <span class="img-holder"><img src="images/chat-3.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Adman Cruz</span>
                              <span class="info">Writer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-away chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user4" class="atag">
                              <span class="img-holder"><img src="images/chat-4.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Elina Grey</span>
                              <span class="info">Cook</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-online chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user1" class="atag active">
                              <span class="img-holder"><img src="images/chat-1.png" /><span class="badge">3</span></span>
                              <span class="desc-holder">
                              <span class="uname">Oliver Rogers</span>
                              <span class="info">Graphics Designer</span>                       
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-away chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user5" class="atag">
                              <span class="img-holder"><img src="images/chat-5.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Salma Musa</span>
                              <span class="info">Photographer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-offline chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user8" class="atag">
                              <span class="img-holder"><img src="images/chat-7.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Kelly Mark</span>
                              <span class="info">Hair Stylist</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-offline chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user9" class="atag">
                              <span class="img-holder"><img src="images/chat-8.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Jason Gomez</span>
                              <span class="info">CEO</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-online chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user3" class="atag">
                              <span class="img-holder"><img src="images/chat-3.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Adman Cruz</span>
                              <span class="info">Writer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="tab-pane fade recentchat-pane" id="chat-recent">
                  <span class="ctitle">Recent</span>
                  <div class="nice-scroll recentchat-scroll">
                     <ul class="chat-away chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user4" class="atag">
                              <span class="img-holder"><img src="images/chat-4.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Elina Grey</span>
                              <span class="info">Cook</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-online chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user1" class="atag active">
                              <span class="img-holder"><img src="images/chat-1.png" /><span class="badge">3</span></span>
                              <span class="desc-holder">
                              <span class="uname">Oliver Rogers</span>
                              <span class="info">Graphics Designer</span>								
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-away chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user5" class="atag">
                              <span class="img-holder"><img src="images/chat-5.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Salma Musa</span>
                              <span class="info">Photographer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-offline chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user8" class="atag">
                              <span class="img-holder"><img src="images/chat-7.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Kelly Mark</span>
                              <span class="info">Hair Stylist</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-offline chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user9" class="atag">
                              <span class="img-holder"><img src="images/chat-8.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Jason Gomez</span>
                              <span class="info">CEO</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-online chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user3" class="atag">
                              <span class="img-holder"><img src="images/chat-3.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Adman Cruz</span>
                              <span class="info">Writer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-away chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user4" class="atag">
                              <span class="img-holder"><img src="images/chat-4.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Elina Grey</span>
                              <span class="info">Cook</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-online chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user1" class="atag active">
                              <span class="img-holder"><img src="images/chat-1.png" /><span class="badge">3</span></span>
                              <span class="desc-holder">
                              <span class="uname">Oliver Rogers</span>
                              <span class="info">Graphics Designer</span>								
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-away chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user5" class="atag">
                              <span class="img-holder"><img src="images/chat-5.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Salma Musa</span>
                              <span class="info">Photographer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-offline chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user8" class="atag">
                              <span class="img-holder"><img src="images/chat-7.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Kelly Mark</span>
                              <span class="info">Hair Stylist</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-offline chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user9" class="atag">
                              <span class="img-holder"><img src="images/chat-8.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Jason Gomez</span>
                              <span class="info">CEO</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                     <ul class="chat-online chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user3" class="atag">
                              <span class="img-holder"><img src="images/chat-3.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Adman Cruz</span>
                              <span class="info">Writer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="tab-pane fade chat_cont_new left" id="chat-online">
                  <span class="ctitle">Online</span>
                  <div class="nice-scroll chatlist-scroll">
                     <ul class="chat-online chat-ul">
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user10" class="atag">
                              <span class="img-holder"><img src="images/chat-1.png" /><span class="badge">3</span></span>
                              <span class="desc-holder">
                              <span class="uname">Oliver Rogers</span>
                              <span class="info">Graphics Designer</span>								
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user11" class="atag">
                              <span class="img-holder"><img src="images/chat-2.png" /><span class="badge">12</span></span>
                              <span class="desc-holder">
                              <span class="uname">Joe Jones</span>
                              <span class="info">Web Designer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user12" class="atag">
                              <span class="img-holder"><img src="images/chat-3.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Adman Cruz</span>
                              <span class="info">Writer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user10" class="atag">
                              <span class="img-holder"><img src="images/chat-1.png" /><span class="badge">3</span></span>
                              <span class="desc-holder">
                              <span class="uname">Oliver Rogers</span>
                              <span class="info">Graphics Designer</span>								
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user11" class="atag">
                              <span class="img-holder"><img src="images/chat-2.png" /><span class="badge">12</span></span>
                              <span class="desc-holder">
                              <span class="uname">Joe Jones</span>
                              <span class="info">Web Designer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user12" class="atag">
                              <span class="img-holder"><img src="images/chat-3.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Adman Cruz</span>
                              <span class="info">Writer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user10" class="atag">
                              <span class="img-holder"><img src="images/chat-1.png" /><span class="badge">3</span></span>
                              <span class="desc-holder">
                              <span class="uname">Oliver Rogers</span>
                              <span class="info">Graphics Designer</span>								
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user11" class="atag">
                              <span class="img-holder"><img src="images/chat-2.png" /><span class="badge">12</span></span>
                              <span class="desc-holder">
                              <span class="uname">Joe Jones</span>
                              <span class="info">Web Designer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user12" class="atag">
                              <span class="img-holder"><img src="images/chat-3.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Adman Cruz</span>
                              <span class="info">Writer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user10" class="atag">
                              <span class="img-holder"><img src="images/chat-1.png" /><span class="badge">3</span></span>
                              <span class="desc-holder">
                              <span class="uname">Oliver Rogers</span>
                              <span class="info">Graphics Designer</span>								
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user11" class="atag">
                              <span class="img-holder"><img src="images/chat-2.png" /><span class="badge">12</span></span>
                              <span class="desc-holder">
                              <span class="uname">Joe Jones</span>
                              <span class="info">Web Designer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user12" class="atag">
                              <span class="img-holder"><img src="images/chat-3.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Adman Cruz</span>
                              <span class="info">Writer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user10" class="atag">
                              <span class="img-holder"><img src="images/chat-1.png" /><span class="badge">3</span></span>
                              <span class="desc-holder">
                              <span class="uname">Oliver Rogers</span>
                              <span class="info">Graphics Designer</span>								
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user11" class="atag">
                              <span class="img-holder"><img src="images/chat-2.png" /><span class="badge">12</span></span>
                              <span class="desc-holder">
                              <span class="uname">Joe Jones</span>
                              <span class="info">Web Designer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user12" class="atag">
                              <span class="img-holder"><img src="images/chat-3.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Adman Cruz</span>
                              <span class="info">Writer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user10" class="atag">
                              <span class="img-holder"><img src="images/chat-1.png" /><span class="badge">3</span></span>
                              <span class="desc-holder">
                              <span class="uname">Oliver Rogers</span>
                              <span class="info">Graphics Designer</span>								
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user11" class="atag">
                              <span class="img-holder"><img src="images/chat-2.png" /><span class="badge">12</span></span>
                              <span class="desc-holder">
                              <span class="uname">Joe Jones</span>
                              <span class="info">Web Designer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                        <li>
                           <div class="chat-summery">
                              <a href="javascript:void(0)" onclick="openChatbox(this)" id="user12" class="atag">
                              <span class="img-holder"><img src="images/chat-3.png" /></span>
                              <span class="desc-holder">
                              <span class="uname">Adman Cruz</span>
                              <span class="info">Writer</span>
                              </span>
                              </a>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="chat-window">
         <a href="javascript:void(0)" class="backChatList" onclick="closeChatboxes()"><i class="mdi mdi-menu-left"></i> Back to list</a>
         <ul class="mainul">
         </ul>
      </div>
   </div>
</div>