<div class="content-box bshadow" id="recently_joined">
   <div class="cbox-title">
      Recently joined
   </div>
   <div class="cbox-desc">
      <div class="recent-list">
         <div class="row">
            <div class="recent-col">
               <div class="recent-box"><a href="javascript:void(0)"><img src="images/recent-1.png"/></a></div>
            </div>
            <div class="recent-col">
               <div class="recent-box"><a href="javascript:void(0)"><img src="images/recent-2.png"/></a></div>
            </div>
            <div class="recent-col">
               <div class="recent-box"><a href="javascript:void(0)"><img src="images/recent-3.png"/></a></div>
            </div>
            <div class="recent-col">
               <div class="recent-box"><a href="javascript:void(0)"><img src="images/recent-4.png"/></a></div>
            </div>
            <div class="recent-col">
               <div class="recent-box"><a href="javascript:void(0)"><img src="images/recent-5.png"/></a></div>
            </div>
            <div class="recent-col">
               <div class="recent-box"><a href="javascript:void(0)"><img src="images/recent-6.png"/></a></div>
            </div>
         </div>
      </div>
   </div>
</div>