<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="main-content with-lmenu sub-page peopleknow-page">
         <div class="combined-column">
            <div class="content-box bshadow">
               <div class="cbox-title">						
                  Search Result
               </div>
               <div class="cbox-desc">
                  <div class="friends-grid freq">
                     <div class="row">
                        <input type="hidden" name="login_id" id="login_id">
                        <input type="hidden" name="to_id" id="to_id">
                        <div class="grid-box">
                           <div class="friend-box">
                              <div class="imgholder online-img">
                                 <img src="images/friends-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="info">AHmedabad</span>
                                 <span class="info">20 Mutual Friends</span>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <div class="friend-box">
                              <div class="imgholder online-img">
                                 <img src="images/friends-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="info">AHmedabad</span>
                                 <span class="info">20 Mutual Friends</span>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-minus"></i>
                                    <i class="dis-none mdi mdi-account-plus"></i>	
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <div class="friend-box">
                              <div class="imgholder">
                                 <img src="images/friends-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="info">AHmedabad</span>
                                 <span class="info">20 Mutual Friends</span>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="MDdropdown gray-text-555" data-activates='travfriends'>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    <i class="dis-none mdi mdi-account-plus"></i>	
                                    <i class="mdi mdi-chevron-down"></i>
                                    </a>
                                    <!-- Dropdown Structure -->
                                    <ul id='travfriends' class='dropdown-content'>
                                       <li><a href="#!">Unfriend</a></li>
                                       <li><a href="#!">View all</a></li>
                                       <li><a href="#!">Suggest friends</a></li>
                                       <li><a href="#!">Mute Notifications</a></li>
                                       <li><a href="#!">Chat</a></li>
                                       <li><a href="#!">Send gift</a></li>
                                       <li><a href="#!">Block</a></li>
                                    </ul>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <div class="friend-box">
                              <div class="imgholder">
                                 <img src="images/friends-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="info">AHmedabad</span>
                                 <span class="info">20 Mutual Friends</span>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <div class="friend-box">
                              <div class="imgholder">
                                 <img src="images/friends-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="info">AHmedabad</span>
                                 <span class="info">20 Mutual Friends</span>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <div class="friend-box">
                              <div class="imgholder">
                                 <img src="images/friends-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="info">AHmedabad</span>
                                 <span class="info">20 Mutual Friends</span>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <div class="friend-box">
                              <div class="imgholder">
                                 <img src="images/friends-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="info">AHmedabad</span>
                                 <span class="info">20 Mutual Friends</span>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <div class="friend-box">
                              <div class="imgholder">
                                 <img src="images/friends-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="info">AHmedabad</span>
                                 <span class="info">20 Mutual Friends</span>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                           <div class="friend-box">
                              <div class="imgholder">
                                 <img src="images/friends-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                 <span>Bhadresh Ramani</span>
                                 </a>
                                 <span class="info">AHmedabad</span>
                                 <span class="info">20 Mutual Friends</span>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="gray-text-555">
                                    <i class="mdi mdi-account-plus"></i>
                                    <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>	
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<?php include("script.php"); ?>
</body>
</html>