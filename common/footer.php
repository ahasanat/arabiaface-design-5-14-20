<div class="footer-section">
   <div class="mobile-footer-arrow">
      <div class="mobile-footer">
         <div class="dropdown dropdown-custom mmenu text-center">
            <a href="index.php" class="dropdown-toggle"  role="button" aria-haspopup="true" aria-expanded="false">
            <img src="images/foot-home.png" />
            Home
            </a>					
         </div>
         <div class="dropdown dropdown-custom mmenu text-center">
            <a href="messages.php" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="images/chat-white.png" />
            Messenger
            </a> 
         </div>
         <div class="dropdown dropdown-custom mmenu text-center">
            <a href="who-is-around.php" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="images/foot-places.png" />
            People
            </a>					
         </div>
         <div class="dropdown-button more_btn footer-menu dropdown-custom mmenu text-center" onclick="openalert()">
            <a href="javascript:void(0)">
               <img src="images/foot-more.png" />
               Alert
            </a>
         </div>
      </div>
      <div class="master_alert">
         <div class="littlemaster_alert">
            <a href="people-you-know.php">
               <span class="icon default-icon">
                  <img src="images/friendreq-icon.png">
               </span>Friend Requests
            </a>
         </div>
         <div class="littlemaster_alert">
            <a href="messages-new.php">
               <span class="icon default-icon">
                  <img src="images/message-icon.png">
               </span>Messages
            </a>
         </div>
         <div class="littlemaster_alert">
            <a href="notifications.php">
               <span class="icon default-icon">
                  <img src="images/notification-icon.png">
               </span>Notifications
            </a>
         </div>
      </div>
   </div>
   <div class="main-footer">
      <ul class="center-align">
         <li><a href="javascript:void(0)">About</a></li>
         <li><a href="javascript:void(0)">Privacy</a></li>
         <li><a href="javascript:void(0)">Invite</a></li>
         <li><a href="javascript:void(0)">Terms</a></li>
         <li><a href="javascript:void(0)">Contact Us</a></li>
         <li><a href="javascript:void(0)">Features</a></li>
         <li><a href="javascript:void(0)">Mobile</a></li>
         <li><a href="javascript:void(0)">Developers</a></li>
      </ul>
   </div>
</div>