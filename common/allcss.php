<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="images/favicon.ico">
<title>Arabiaface</title>
<link rel="icon" href="images/favicon.ico">
<link href="css/materialize.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/nouislider.css" rel="stylesheet">
<link href="css/material-design-iconic-font.css" rel="stylesheet">
<link href="css/materialdesignicons.min.css" rel="stylesheet">    
<link href="css/tooltipster.bundle.min.css" rel="stylesheet">
<link href="css/tooltipster-sideTip-borderless.min.css" rel="stylesheet">
<link href="css/emoticons.css" rel="stylesheet">
<link href="css/emostickers.css" rel="stylesheet">
<link href="css/demo-cover.css" type="text/css" media="screen" rel="stylesheet" />
<link href="css/jquery-gauge.css" type="text/css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/template.css" rel="stylesheet">
<link href="css/themes.css" rel="stylesheet">
<link href="css/all-ie-only.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="css/master-responsive.css" rel="stylesheet">
<link href="css/custom-croppie.css" rel="stylesheet">
<link href="css/datepicker.min.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>