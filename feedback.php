
<?php include("header.php"); ?>
<div class="main-content text-center">
   <div class="feedback-notice">
      <span class="success-note">Successfully Logged in!</span>
      <span class="info-note">Fill in the mandatory fields.</span>
      <span class="error-note">Please Enter Email address and Password</span>
   </div>
   <div class="feedback-box bshadow">
      <div class="feedback-success">
         <h5>Your feedback for <span>Adel Hasanat</span> has been successfully submitted!</h5>
         <div class="clear"></div>
         <img src="images/feedback-submit.png"/>
         <div class="clear"></div>
         <input type="submit" class="btn btn-primary" value="Login Now"/>
      </div>
      <div class="feedback-form">
         <h5>Please provide your feedback for "Adel Hasanat"</h5>
         <form>
            <div class="dropdown dropdown-custom feedback-drop setDropVal">
               <a href="javascript:void(0)" class="dropdown-toggle"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
               <span class="pc-text">Select Feedback</span>
               <span class="caret"></span>
               </a>
               <ul class="dropdown-menu">
                  <li><a href="javascript:void(0)"><i class="mdi mdi-plus-square"></i>Positive</a></li>
                  <li><a href="javascript:void(0)"><i class="mdi mdi-bullseye"></i></i>Neutral</a></li>
                  <li><a href="javascript:void(0)"><i class="mdi mdi-close-circle-outline"></i>Negative</a></li>
               </ul>
            </div>
            <div class="clear"></div>
            <label>Write your comment here</label>
            <textarea class="materialize-textarea"></textarea>
            <input type="submit" class="btn btn-primary" value="Submit"/>
         </form>
      </div>
   </div>
</div>
</div>	
<?php include("script.php"); ?>
<script type="text/javascript">
   $(document).ready(function(){
   	
   	setSinglePageHeight();
   });
</script>
</body>
</html>