<div class="content-box bshadow peopleyoumayknow">
   <div class="cbox-title">
      People you may know
   </div>
   <div class="cbox-desc">
      <ul class="people-list">
         <li>
            <div class="people-box">
               <div class="img-holder"><img src="images/people-1.png"/></div>
               <div class="desc-holder">
                  <a href="javascript:void(0)" class="userlink">Ali Musa</a>
                  <span class="info">23 mutual friends</span>
                  <a href="javascript:void(0)" class="add-user-icon">
                  <i class="add_friend mdi mdi-account-plus"></i>	
                  <i class="remove_friend mdi mdi-account-minus"></i>
                  </a>
                  <a href="javascript:void(0)" class="close-btn">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
            </div>
         </li>
         <li>
            <div class="people-box">
               <div class="img-holder"><img src="images/people-2.png"/></div>
               <div class="desc-holder">
                  <a href="javascript:void(0)" class="userlink">Adel Google</a>
                  <span class="info">5 mutual friends</span>
                  <a href="javascript:void(0)" class="add-user-icon">
                  <i class="add_friend mdi mdi-account-plus"></i>	
                  <i class="remove_friend mdi mdi-account-minus"></i>
                  </a>
                  <a href="javascript:void(0)" class="close-btn">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
            </div>
         </li>
         <li>
            <div class="people-box">
               <div class="img-holder"><img src="images/people-3.png"/></div>
               <div class="desc-holder">
                  <a href="javascript:void(0)" class="userlink">Ada Hasanat</a>
                  <span class="info">51 mutual friends</span>
                  <a href="javascript:void(0)" class="add-user-icon">
                  <i class="add_friend mdi mdi-account-plus"></i>	
                  <i class="remove_friend mdi mdi-account-minus"></i>
                  </a>
                  <a href="javascript:void(0)" class="close-btn">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
            </div>
         </li>
         <li class="viewall">
            <div class="right"><a href="javascript:void(0)">View All</a></div>
         </li>
      </ul>
   </div>
</div>