<?php
	$mhidecls = '';
	$mHidePageBulks = array('places','trip','credit-transfer','credit-update','credit','verify','vip-member','billing-info','settings','ad-manager','advertisement','manage-ad','hotels','tours','wall','pages','messages-org','messages','business-page');	
	if(in_array($file, $mHidePageBulks)) {
		$mhidecls = 'm-hide';
	}
?>
<div class="sidemenu-holder <?=$mhidecls?>">
   <div class="sidemenu nice-scroll">
      <a href="javascript:void(0)" class="closemenu waves-theme waves-effect"><i class="mdi mdi-close"></i></a>
      <div class="side-user-cover">
         <img src="images/wgallery3.jpg">
      </div>
      <div class="side-user">
         <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
         <a href="wall"><span class="desc-holder">Nimish Parekh</span></a>
      </div>
      <div class="sidemenu-ul">
		<ul>
			<li class="lm-home<?=($file=="index")?' active' :'';?>"><a href="index.php">Home</a></li>
			<li class="lm-messages <?=($file=="messages")?' active' :'';?>"><a href="messages.php">Messenger</a></li>
			<li class="lm-waround<?=($file=="who-is-around")?' active' :'';?>"><a href="who-is-around.php">People</a></li>
			<li class="lm-channels<?=($file=="channels" || $file=="channels-detail")?' active' :'';?>"><a href="channels.php">Channels</a></li>
			<li class="lm-collections<?=($file=="collections" || $file=="collections-detail")?' active' :'';?>"><a href="collections.php">Collections</a></li>
			<li class="lm-snapit<?=($file=="snapit")?' active' :'';?>"><a href="snapit.php">Snapit</a></li>
			<li class="lm-texp<?=($file=="moments")?' active' :'';?>"><a href="moments.php">Moments</a></li>
			<li class="lm-commeve<?=($file=="community-events" || $file=="community-events-detail")?' active' :'';?>"><a href="community-events.php">Events</a></li>
			<li class="lm-groups<?=($file=="groups" || $file=="groups-detail")?' active' :'';?>"><a href="groups.php">Groups</a></li>
			<li class="lm-pages<?=($file=="pages")?' active' :'';?>"><a href="pages.php">Pages</a></li>
		</ul>
	</div>
   </div>
   <div class="mobile-menu">
      <a href="javascript:void(0)"><i class="mdi mdi-menu"></i></a>
   </div>
</div>