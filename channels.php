<!DOCTYPE html>
<html lang="en"> 
   <head>
      <?php include("common/allcss.php"); ?>   
   </head>
   <body class="theme-color">
      <div class="page-wrapper gen-pages"> 
         <div class="header-section">
            <?php include("header1.php"); ?>
         </div>
         <div class="floating-icon">
            <div class="scrollup-btnbox anim-side btnbox scrollup-float">
               <div class="scrollup-button float-icon">
                  <span class="icon-holder ispan">
                     <i class="mdi mdi-arrow-up-bold-circle"></i>
                  </span>
               </div>
            </div>
         </div> 
         <div class="clear"></div>
         <div class="container page_container channel_container">
            <?php include("common/leftmenu.php"); ?>
            <div class="fixed-layout ipad-mfix">
               <div class="main-content with-lmenu channels-page main-page grid-view general-page">
                  <div class="combined-column comsepcolumn">
                     <div class="content-box nbg">
                        <div class="cbox-desc md_card_tab">
                           <div class="fake-title-area divided-nav mobile-header">
                              <ul class="tabs">
                                 <li class="tab col s3">
                                    <a class="active" href="#channels-suggested" data-toggle="tab" aria-expanded="true">Suggested</a>
                                 </li>
                                 <li class="tab col s3">
                                    <a href="#channels-subscribed" data-toggle="tab" aria-expanded="false">Subscribed</a>
                                 </li>
                                 <li class="tab col s3">
                                    <a href="#channels-popular" data-toggle="tab" aria-expanded="false">Popular</a>
                                 </li>
                              </ul>
                           </div>
                           <div class="tab-content view-holder grid-view">
                              <div class="tab-pane fade main-pane active in" id="channels-suggested">
                                 <div class="generalbox-list channels-list">
                                    <div class="row">
                                       <div class="col s6 m4 l3 gridBox127">
                                          <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="channels-detail.php" class="general-box">
                                                <div class="photo-holder waves-effect waves-block waves-light">
                                                   <img src="images/additem-channels.png">
                                                </div>
                                                <div class="content-holder">
                                                   <h4>Exploring World</h4>
                                                   <div class="icon-line">
                                                      <span>Tagline goes here</span>
                                                   </div>
                                                   <div class="countinfo">
                                                      1
                                                   </div>
                                                   <div class="posts-line">
                                                      230 posts
                                                   </div>
                                                   <div class="action-btns">
                                                      <span class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col s6 m4 l3 gridBox127">
                                          <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="channels-detail.php" class="general-box">
                                                <div class="photo-holder waves-effect waves-block waves-light">
                                                   <img src="images/channel-img-1.png">
                                                </div>
                                                <div class="content-holder">
                                                   <h4>Paris Eye</h4>
                                                   <div class="icon-line">
                                                      <span>About planning a Paris tour</span>
                                                   </div>
                                                   <div class="countinfo">
                                                      2
                                                   </div>
                                                   <div class="posts-line">
                                                      230 posts
                                                   </div>
                                                   <div class="action-btns">
                                                      <span class="noClick" onclick="channelSubscribe(event,'channel2',this)">Subscribe</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col s6 m4 l3 gridBox127">
                                          <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="channels-detail.php" class="general-box">
                                                <div class="photo-holder waves-effect waves-block waves-light">
                                                   <img src="images/channel-img-2.png">
                                                </div>
                                                <div class="content-holder">
                                                   <h4>Beaches To Travel</h4>
                                                   <div class="icon-line">
                                                      <span>7 awesome beaches across the world</span>
                                                   </div>
                                                   <div class="countinfo">
                                                      3
                                                   </div>
                                                   <div class="posts-line">
                                                      230 posts
                                                   </div>
                                                   <div class="action-btns">
                                                      <span class="noClick" onclick="channelSubscribe(event,'channel3',this)">Subscribe</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col s6 m4 l3 gridBox127">
                                          <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="channels-detail.php" class="general-box">
                                                <div class="photo-holder waves-effect waves-block waves-light">
                                                   <img src="images/channel-img-3.png">
                                                </div>
                                                <div class="content-holder">
                                                   <h4>Manage Travel Budget</h4>
                                                   <div class="icon-line">
                                                      <span>Tips to manage your travel budget</span>
                                                   </div>
                                                   <div class="countinfo">
                                                      4
                                                   </div>
                                                   <div class="posts-line">
                                                      230 posts
                                                   </div>
                                                   <div class="action-btns">
                                                      <span class="noClick disabled" onclick="channelSubscribe(event,'channel1',this)">Unsubscribe</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane fade main-pane dis-none" id="channels-subscribed">
                                 <div class="generalbox-list channels-list">
                                    <div class="row">
                                       <div class="col s6 m4 l3 gridBox127">
                                          <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="channels-detail.php" class="general-box">
                                                <div class="photo-holder waves-effect waves-block waves-light">
                                                   <img src="images/channel-img-3.png">
                                                </div>
                                                <div class="content-holder">
                                                   <h4>Manage Travel Budget</h4>
                                                   <div class="icon-line">
                                                      <span>Tips to manage your travel budget</span>
                                                   </div>
                                                   <div class="countinfo">
                                                      4
                                                   </div>
                                                   <div class="posts-line">
                                                      230 posts
                                                   </div>
                                                   <div class="action-btns">
                                                      <span class="noClick disabled" onclick="channelSubscribe(event,'channel1',this)">Unsubscribe</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane fade main-pane general-yours dis-none" id="channels-popular">
                                 <div class="generalbox-list channels-list">
                                    <div class="row">
                                       <div class="col s6 m4 l3 gridBox127">
                                          <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="channels-detail.php" class="general-box">
                                                <div class="photo-holder waves-effect waves-block waves-light">
                                                   <img src="images/additem-channels.png">
                                                </div>
                                                <div class="content-holder">
                                                   <h4>Exploring World</h4>
                                                   <div class="icon-line">
                                                      <span>Tagline goes here</span>
                                                   </div>
                                                   <div class="countinfo">
                                                      1
                                                   </div>
                                                   <div class="posts-line">
                                                      230 posts
                                                   </div>
                                                   <div class="action-btns">
                                                      <span class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col s6 m4 l3 gridBox127">
                                          <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="channels-detail.php" class="general-box">
                                                <div class="photo-holder waves-effect waves-block waves-light">
                                                   <img src="images/channel-img-1.png">
                                                </div>
                                                <div class="content-holder">
                                                   <h4>Paris Eye</h4>
                                                   <div class="icon-line">
                                                      <span>About planning a Paris tour</span>
                                                   </div>
                                                   <div class="countinfo">
                                                      2
                                                   </div>
                                                   <div class="posts-line">
                                                      230 posts
                                                   </div>
                                                   <div class="action-btns">
                                                      <span class="noClick" onclick="channelSubscribe(event,'channel2',this)">Subscribe</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col s6 m4 l3 gridBox127">
                                          <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="channels-detail.php" class="general-box">
                                                <div class="photo-holder waves-effect waves-block waves-light">
                                                   <img src="images/channel-img-3.png">
                                                </div>
                                                <div class="content-holder">
                                                   <h4>Manage Travel Budget</h4>
                                                   <div class="icon-line">
                                                      <span>Tips to manage your travel budget</span>
                                                   </div>
                                                   <div class="countinfo">
                                                      4
                                                   </div>
                                                   <div class="posts-line">
                                                      230 posts
                                                   </div>
                                                   <div class="action-btns">
                                                      <span class="noClick disabled" onclick="channelSubscribe(event,'channel1',this)">Unsubscribe</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php include('common/chat.php'); ?>
               </div>
            </div>
         </div>
         <?php include("common/footer.php"); ?>
      </div>
   </body>
   <?php include("script.php"); ?>
</body>
</html>