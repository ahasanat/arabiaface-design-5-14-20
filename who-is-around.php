<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container group_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="main-content with-lmenu  whoisaround-page general-page main-page">
         <div class="combined-column comsepcolumn">
            <div class="content-box nbg">
               <div class="cbox-desc md_card_tab">
                  <div class="fake-title-area divided-nav mobile-header">
                     <ul class="tabs">
                        <li class="active tab" onclick="allwhoisaroundusers();"><a href="#whoaround-online">All People</a></li>
                        <li onclick="allfriends();" class="tab"><a href="#whoaround-allfriends">Friends</a></li>
                        <li class="find_friend tab"><a href="#whoaround-findfriends">Find Friends</a></li>
                     </ul>
                  </div>
                  <div class="filters">
                     <div class="friends-search shifted">
                        <div class="fsearch-form closable-search">
                           <input placeholder="Search for your friends" type="text">
                           <a href="javascript:void(0)" class="gray-text-555"><i class="mdi mdi-magnify mdi-16px"></i></a>
                        </div>
                     </div>
                     <div class="filter-area expandable-holder custome-who">
                        <a href="javascript:void(0)" class="expand-link gray-text-555" onclick="mng_expandable(this,'none')">
                           <i class="mdi mdi-tune mdi-20px"></i>
                        </a>
                        <div class="main-collape">
                           <div class="form-area expandable-area">
                              <a href="javascript:void(0)" class="close-link" onclick="mng_expandable(this,'none')">DONE</a>
                              <h6>Refine Results</h6> 
                              <form class="howisform">
                                 <div class="fitem">
                                    <div class="entertosend leftbox">
                                       <input id="Online1" type="checkbox">
                                       <label for="Online1">Online</label>
                                    </div>
                                 </div>
                                 <div class="fitem">
                                    <div class="entertosend leftbox">
                                       <input id="Birthday" type="checkbox">
                                       <label for="Birthday">Birthday</label>
                                    </div>
                                 </div>
                                 <div class="fitem age-filter">
                                    <label>Age</label>
                                    <div class="desc">
                                       <div class="range-slider">
                                          <div id="test-slider"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="fitem selectholder">
                                    <label>Gender</label>
                                    <div class="custom-select">
                                       <select class="target">
                                          <option>All</option>
                                          <option>Male</option>
                                          <option>Female</option>
                                       </select>
                                    </div>
                                 </div>
                                 <script type="text/javascript">
                                    $( ".select-dropdown" ).click(function() {
                                      alert( "Handler test" );
                                    });
                                 </script>
                                 <div class="fitem near-who">
                                    <label>Near by</label>
                                    <div class="desc">
                                       <div class="fsearch-form">
                                          <input type="text" placeholder="Type your location" data-query="M" onfocus="filderMapLocationModal(this)" autocomplete="off"/>
                                          <a href="javascript:void(0)"><i class="zmdi zmdi-search"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="tab-content view-holder grid-view custom-tab-who">
                     <div class="tab-pane fade main-pane active in whoaround-online-new whoaround-online" id="whoaround-online">
                        <div class="animated fadeInUp">
                           <div class="person-list ">
                              <div class="row">
                                 <div class="col s12 m3 l3 xl3">
                                    <div class="person-box ">
                                       <div class="imgholder online-img">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="add-span ">
                                                <a href="javascript:void(0)" class="add-icon request_btn gray-text-555"><i class="mdi mdi-account-plus"></i></a>
                                                <a href="javascript:void(0)" class="add-btn btn btn-primary" onclick="addFriendBtn(this)">Add Friend</a>
                                             </div>
                                             <div class="more-span mobile-none-who">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="who_online1"><i class="mdi mdi-chevron-down"></i>
                                                   </a>
                                                   <ul id="who_online1" class="dropdown-content custom_dropdown">
                                                      <li><a class="request_btn" href="javascript:void(0)">Add friend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Send message</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m3 l3 xl3">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img class="main-img" src="images/demo-profile.jpg"/>
                                          <div class="overlay">
                                             <div class="add-span ">
                                                <a href="javascript:void(0)" class="add-icon Cancelling_btn gray-text-555"><i class="mdi mdi-account-minus"></i></a>
                                             </div>
                                             <div class="vip-span"><img src="images/vip-tag.png"/></div>
                                             <div class="more-span mobile-none-who">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="who_online2"><i class="mdi mdi-chevron-down"></i></a>
                                                   <ul id="who_online2" class="dropdown-content custom_dropdown cancle-popu">
                                                      <li><a class="Cancelling_btn" href="javascript:void(0)">Cancle friend request</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Send message</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m3 l3 xl3">
                                    <div class="person-box">
                                       <div class="imgholder online-img">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="add-span">
                                                <a href="javascript:void(0)" class="add-icon request_btn gray-text-555"><i class="mdi mdi-account-plus"></i></a>
                                                <a href="javascript:void(0)" class="add-btn btn btn-primary " onclick="addFriendBtn(this)">Add Friend</a>
                                             </div>
                                             <div class="more-span mobile-none-who">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0)" data-activates="who_online3"><i class="mdi mdi-chevron-down"></i></a>
                                                   <ul id="who_online3" class="dropdown-content custom_dropdown">
                                                      <li><a class="request_btn" href="javascript:void(0)">Add friend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Send message</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m3 l3 xl3">
                                    <div class="person-box">
                                       <div class="imgholder online-img">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0)" data-activates="who_online4" ><i class="mdi mdi-chevron-down"></i></a>
                                                   <ul id="who_online4" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a class="suggest-friends" href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a class="mute" onclick="$('.unmute').show();$('.mute').hide();" href="javascript:void(0)">Mute notifications</a>
                                                         <a style="display:none" class="unmute" onclick="$('.mute').show();$('.unmute').hide();" href="javascript:void(0)">Unmute notifications</a>
                                                      </li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li>
                                                         <a class="block" onclick="$('.unblock').show();$('.block').hide();" href="javascript:void(0)">Block</a>
                                                         <a style="display:none" class="unblock" onclick="$('.block').show();$('.unblock').hide();" href="javascript:void(0)">Unblock</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m3 l3 xl3">
                                    <div class="person-box">
                                       <div class="imgholder ">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="vip-span"><img src="images/vip-tag.png"/></div>
                                             <div class="add-span">
                                                <a href="javascript:void(0)" class="add-icon request_btn gray-text-555"><i class="mdi mdi-account-plus"></i></a>
                                                <a href="javascript:void(0)" class="add-btn btn btn-primary " onclick="addFriendBtn(this)">Add Friend</a>
                                             </div>
                                             <div class="more-span mobile-none-who">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0)" data-activates="who_online5" ><i class="mdi mdi-chevron-down"></i></a>
                                                   <ul id="who_online5" class="dropdown-content custom_dropdown">
                                                      <li><a class="request_btn" href="javascript:void(0)">Add friend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Send message</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m3 l3 xl3">
                                    <div class="person-box">
                                       <div class="imgholder online-img">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="vip-span"><img src="images/vip-tag.png"/></div>
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0)" data-activates="who_online6"><i class="mdi mdi-chevron-down"></i></a>
                                                   <ul id="who_online6" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a href="javascript:void(0)">Mute notifications</a></li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m3 l3 xl3">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0)" data-activates="who_online7"><i class="mdi mdi-chevron-down"></i></a>
                                                   <ul id="who_online7" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a href="javascript:void(0)">Mute notifications</a></li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m3 l3 xl3">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="add-span">
                                                <a href="javascript:void(0)" class="add-icon request_btn gray-text-555"><i class="mdi mdi-account-plus"></i></a>
                                                <a href="javascript:void(0)" class="add-btn btn btn-primary " onclick="addFriendBtn(this)">Add Friend</a>
                                             </div>
                                             <div class="more-span mobile-none-who">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0)" data-activates="who_online8"><i class="mdi mdi-chevron-down"></i></a>
                                                   <ul id="who_online8" class="dropdown-content custom_dropdown">
                                                      <li><a class="request_btn" href="javascript:void(0)">Add friend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Send message</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane dis-none whoaround-allfriends" id="whoaround-allfriends">
                        <div class="animated fadeInUp">
                           <div class="person-list">
                              <div class="row">
                                 <div class="col s12 m4 l2 xl2">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="who_frnd1"><i class="mdi mdi-chevron-down"></i>
                                                   </a>
                                                   <ul id="who_frnd1" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a href="javascript:void(0)">Mute notifications</a></li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m4 l2 xl2">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="vip-span"><img src="images/vip-tag.png"/></div>
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="who_frnd2"><i class="mdi mdi-chevron-down"></i>
                                                   </a>
                                                   <ul id="who_frnd2" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a href="javascript:void(0)">Mute notifications</a></li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m4 l2 xl2">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="who_frnd3"><i class="mdi mdi-chevron-down"></i>
                                                   </a>
                                                   <ul id="who_frnd3" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a href="javascript:void(0)">Mute notifications</a></li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m4 l2 xl2">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="who_frnd4"><i class="mdi mdi-chevron-down"></i>
                                                   </a>
                                                   <ul id="who_frnd4" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a href="javascript:void(0)">Mute notifications</a></li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m4 l2 xl2">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="vip-span"><img src="images/vip-tag.png"/></div>
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="who_frnd5"><i class="mdi mdi-chevron-down"></i>
                                                   </a>
                                                   <ul id="who_frnd5" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a href="javascript:void(0)">Mute notifications</a></li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m4 l2 xl2">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="vip-span"><img src="images/vip-tag.png"/></div>
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="who_frnd6"><i class="mdi mdi-chevron-down"></i>
                                                   </a>
                                                   <ul id="who_frnd6" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a href="javascript:void(0)">Mute notifications</a></li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m4 l2 xl2">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="who_frnd7"><i class="mdi mdi-chevron-down"></i>
                                                   </a>
                                                   <ul id="who_frnd7" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a href="javascript:void(0)">Mute notifications</a></li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col s12 m4 l2 xl2">
                                    <div class="person-box">
                                       <div class="imgholder">
                                          <img src="images/demo-profile.jpg" class="main-img"/>
                                          <div class="overlay">
                                             <div class="more-span">
                                                <div class="dropdown dropdown-custom ">
                                                   <a class="dropdown-button more_btn gray-text-555" href="javascript:void(0);" data-activates="who_frnd8"><i class="mdi mdi-chevron-down"></i>
                                                   </a>
                                                   <ul id="who_frnd8" class="dropdown-content custom_dropdown">
                                                      <li><a href="javascript:void(0)">Unfriend</a></li>
                                                      <li><a href="javascript:void(0)">View wall</a></li>
                                                      <li><a href="javascript:void(0)">Suggest friends</a></li>
                                                      <li><a href="javascript:void(0)">Mute notifications</a></li>
                                                      <li><a href="javascript:void(0)">Chat</a></li>
                                                      <li><a href="javascript:void(0)">Send gift</a></li>
                                                      <li><a href="javascript:void(0)">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="descholder">
                                          <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                          <p>Lives in London</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane dis-none" id="whoaround-findfriends">
                        <div class="text-center fullwidth">
                           <div class="groupCard animated fadeInUp">
                              <div class="find-friends">
                                 <h3>Invite your friends</h3>
                                 <h5>Get started by choosing a service provider</h5>
                                 <div class="clear"></div>
                                 <ul class="providers">
                                    <li>
                                       <span class="imgholder"><img src="images/provider-fb.png"/></span>
                                       Facebook
                                       <a href="javascript:void(0)" class="btn btn-gray popup-modal suggest-friends">Invite</a>
                                    </li>
                                    <li>
                                       <span class="imgholder"><img src="images/provider-gp.png"/></span>
                                       Google+
                                       <a href="javascript:void(0)" class="btn btn-gray popup-modal suggest-friends">Invite</a>
                                    </li>
                                    <li>
                                       <span class="imgholder"><img src="images/provider-yahoo.png"/></span>
                                       Yahoomail
                                       <a href="javascript:void(0)" class="btn btn-gray popup-modal suggest-friends">Invite</a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<div id="suggest-friends" class="modal tbpost_modal custom_modal split-page main_modal"">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <h3>Suggest friends</h3>
         </div>
         <div class="custom_modal_content modal_content">
            <div class="hangout-yours profile-tab">
               <div class="hangout-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <div class="friends-grid suggest-friend-list">
                                 <div class="grid-box">
                                    <div class="friend-box">
                                       <div class="imgholder">
                                          <img src="images/friends-1.png"/>
                                       </div>
                                       <div class="descholder">
                                          <a href="javascript:void(0)" class="userlink"><span>Ger Brian</span></a>
                                          <div class="btns-holder desc-menu">
                                             <a class="btn btn-primary btn-sm" href="javascript:void(0)">Suggest Friend</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="grid-box">
                                    <div class="friend-box">
                                       <div class="imgholder">
                                          <img src="images/friends-1.png"/>
                                       </div>
                                       <div class="descholder">
                                          <a href="javascript:void(0)" class="userlink"><span>Ger Brian</span></a>
                                          <div class="btns-holder desc-menu">
                                             <a class="btn btn-primary btn-sm" href="javascript:void(0)">Suggest Friend</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="grid-box">
                                    <div class="friend-box">
                                       <div class="imgholder">
                                          <img src="images/friends-1.png"/>
                                       </div>
                                       <div class="descholder">
                                          <a href="javascript:void(0)" class="userlink"><span>Ger Brian</span></a>
                                          <div class="btns-holder desc-menu">
                                             <a class="btn btn-primary btn-sm" href="javascript:void(0)">Suggest Friend</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="grid-box">
                                    <div class="friend-box">
                                       <div class="imgholder">
                                          <img src="images/friends-1.png"/>
                                       </div>
                                       <div class="descholder">
                                          <a href="javascript:void(0)" class="userlink"><span>Ger Brian</span></a>
                                          <div class="btns-holder desc-menu">
                                             <a class="btn btn-primary btn-sm" href="javascript:void(0)">Suggest Friend</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="grid-box">
                                    <div class="friend-box">
                                       <div class="imgholder">
                                          <img src="images/friends-1.png"/>
                                       </div>
                                       <div class="descholder">
                                          <a href="javascript:void(0)" class="userlink"><span>Ger Brian</span></a>
                                          <div class="btns-holder desc-menu">
                                             <a class="btn btn-primary btn-sm" href="javascript:void(0)">Suggest Friend</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="grid-box">
                                    <div class="friend-box">
                                       <div class="imgholder">
                                          <img src="images/friends-1.png"/>
                                       </div>
                                       <div class="descholder">
                                          <a href="javascript:void(0)" class="userlink"><span>Ger Brian</span></a>
                                          <div class="btns-holder desc-menu">
                                             <a class="btn btn-primary btn-sm" href="javascript:void(0)">Suggest Friend</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal">Cancel</a>
   </div>
</div>
<?php include('common/discard_popup.php'); ?>
</div>
<?php include("script.php"); ?>
</body>
</html>
