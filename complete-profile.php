<?php include("header.php"); ?>
<a href="javascript:void(0)" class="btn btn-custom text-center Complete_Pro">Complete your profile</a>
<a href="javascript:void(0)" class="btn btn-custom text-center Complete_loged">loged</a>
<?php include("common/footer.php"); ?>				
<div id="Complete_Pro" class="modal tbpost_modal custom_modal split-page complete-popup open">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)"></a>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="comp_popup profile-tab">
               <div class="comp_popup_box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="hangoutevent-form">
                                 <div class="combined-column wide-open">
                                    <div class="complete-profile-page">
                                       <div class="complete-content">
                                          <div class="signup-part">
                                             <div class="signup-box " id="create-account">
                                                <div class="complete-profile-header">
                                                   <h5 class="m-0">Welcome Adel Hasanat</h5>
                                                   <p><i>Let's complete your profile</i></p>
                                                   <img src="images/friends-1.png" class="center-block circle" alt="img">
                                                </div>
                                                <div class="box-content">
                                                   <div class="signup-notice">
                                                      <span class="success-note">Successfully Logged in!</span>
                                                      <span class="info-note">Fill in the mandatory fields.</span>
                                                      <span class="error-note">Please Enter Email address and Password</span>
                                                   </div>
                                                   <div class="bc-row">
                                                      <div class="bc-component">
                                                         <div class="sliding-middle-out anim-area underlined">
                                                            <input data-query="M" id="icon_telephone" class="validate" onfocus="filderMapLocationModal(this)" autocomplete="off" placeholder="City" type="text">
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="bc-row">
                                                      <div class="bc-component">
                                                         <div class="sliding-middle-out anim-area underlined">
                                                            <input type="text" placeholder="Country">
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="bc-row">
                                                      <div class="bc-component">
                                                         <div class="sliding-middle-out anim-area underlined">
                                                            <select id="genderDrop" class="select2 genderDrop" >
                                                               <option>Gender</option>
                                                               <option>Male</option>
                                                               <option>Female</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="bc-row">
                                                      <div class="bc-component">
                                                         <div class="sliding-middle-out anim-area underlined">
                                                            <input type="text" onkeydown="return false;" placeholder="Birthdate" name="LoginForm[birth_date]" data-toggle="datepicker" class="datepickerinput form-control" data-query="M" id="datepicker" readonly>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="nextholder center">
                                                   <a href="javascript:void(0)" class="profilebtn waves-effect">Continue</a>
                                                   <!-- <span class="pnote">* All the fields are mandatory</span> -->
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="Complete_loged" class="modal tbpost_modal custom_modal split-page complete-popup complete-loged">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)"></i></a>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="comp_popup profile-tab">
               <div class="comp_popup_box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="hangoutevent-form">
                                 <div class="combined-column wide-open">
                                    <div class="complete-profile-page">
                                       <div class="complete-content">
                                          <div class="signup-part">
                                             <div class="signup-box " id="create-account">
                                                <div class="complete-profile-header">
                                                   <h5 class="m-0">Welcome Back</h5>
                                                   <p><i>Adel, logining in....</i></p>
                                                   <img src="images/friends-1.png" class="center-block circle" alt="img">
                                                </div>
                                                <div class="box-content">
                                                   <center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling105 dis-none"> <div></div> </div></div></center>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php include('common/datepicker.php'); ?>
<?php include("script.php"); ?>
</body>
</html>