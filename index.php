
<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
         <i class="mdi mdi-arrow-up-bold-circle"></i>

         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="main-content with-lmenu fullmobile feedpage">
         <div class="post-column mr-top">
            <?php include('common/new_post.php'); ?>   
         </div>
         <div class="scontent-column">
            <?php include('common/people_you_may_know.php'); ?>
            <div class="content-box bshadow">
               <div class="cbox-title">
                  How often people viewed you
               </div>
               <div class="cbox-desc">
                  <p>16 Profile Views</p>
                  <img src="images/graph.png"/>
               </div>
            </div>
            <?php include('common/recently_joined.php'); ?>
         </div>
         <?php include('common/chat.php'); ?>
      </div>
      <div class="new-post-mobile clear">
         <a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>		
</div>	
<!--map modal-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include('common/privacymodal.php'); ?>
<?php include('common/custom_modal.php'); ?>
<?php include('common/uploadphoto_popup.php'); ?>
<?php include('common/addperson_popup.php'); ?>
<?php include('common/compose_post_popup.php'); ?>
<?php include('common/comment_popup.php'); ?>
<?php include('common/postopen_popup.php'); ?>
<?php include('common/share_popup.php'); ?>
<?php include('common/editpost_popup.php'); ?>
<?php include('common/discard_popup.php'); ?>

<?php include("script.php"); ?>