<!--user gift modal-->
<div id="usegift-popup" class="modal usegift_modal popup-area giftlist-popup giftslider-popup">
   <div class="edit-emosticker">
      <div class="custom_message_modal_header">
         <p>
            Send a gift to <span class="to_gift">Vipul Patel</span>
         </p>
         <button class="close_modal_icon waves-effect" onclick="usegift_modal()">
         <i class="mdi mdi-close mdi-20px material_close "></i>
         </button>
      </div>
      <div class="usergift_content">
         <div class="popup-content">
            <div class="more-friends">
               <p class="send_to">Also send to</p>
               <a><i id="compose_addpersonAction" class="zmdi zmdi-account-add dp48 custome-plus"></i></a>
            </div>
            <div class="carousel_position">
               <div class="carousel carousel-slider center" data-indicators="true">
                  <div class="carousel-fixed-item center middle-indicator">
                     <div class="left">
                        <a href="Previo" class="movePrevCarousel middle-indicator-text waves-effect waves-light content-indicator"><i class="zmdi zmdi-chevron-left left  middle-indicator-text"></i></a>
                     </div>
                     <div class="right">
                        <a href="Siguiente" class=" moveNextCarousel middle-indicator-text waves-effect waves-light content-indicator"><i class="zmdi zmdi-chevron-right right middle-indicator-text"></i></a>
                     </div>
                  </div>
                  <div class="carousel-item white white-text" href="#one!">
                     <img src="images/es-flower.png"/>
                  </div>
                  <div class="carousel-item white white-text" href="#two!">
                     <img src="images/es-handshake.png"/>
                  </div>
                  <div class="carousel-item white white-text" href="#three!">
                     <img src="images/es-cake.png"/>
                  </div>
                  <div class="carousel-item white white-text" href="#four!">
                     <img src="images/es-handshake.png"/>
                  </div>
               </div>
            </div>
            <div class="custom_textarea usergift_msg">
               <textarea id="" class="materialize-textarea" placeholder="Type your message here"></textarea>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <div class="gift_send">
            <a href="javascript:void(0)" class="btn btn-sm waves-effect waves-light">Send</a>
         </div>
      </div>
   </div>
   <div class="preview-emosticker">
      <div class="custom_message_modal_header">
         <p>
            You got a gift from <span>Vipul Patel</span>
         </p>
         <button class="close_modal_icon close_modal">
         <i class="mdi mdi-close mdi-20px material_close "></i>
         </button>
      </div>
      <div class="usergift_content">
         <div class="scroll_div">
            <div class="popup-content">
               <div class="emostickers-holder data-loading">
                  <div class="loading-holder">
                  </div>
               </div>
               <div class="btn-holder">
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <div class="gift_send">
            <a href="javascript:void(0)" class="pull-left close_modal" >Say thank you</a>
            <a href="javascript:void(0)" class="right close_modal" data-dismiss="modal"  onclick="giftModalAction()">Reply with a gift</a>
         </div>
      </div>
   </div>
</div>