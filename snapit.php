<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div> 
<div class="container page_container pages_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="main-content with-lmenu pages-page main-page grid-view general-page">
         <div class="combined-column combined_md_column">
            <div class="content-box nbg">
               <div class="cbox-desc md_card_tab">
                 <div class="row subheader_i subheader_first">
                     <div class="col s8 m6 l5 upload_share_labelarea">
                        <h5>Upload and share photo</h5>
                     </div>
                     <div class="col s7 l4 searcharea">
                        <div class="searchbox">
                           <input type="text" placeholder="Search for photos by title">
                           <i class="mdi mdi-magnify"></i>
                        </div>
                     </div>
                     <div class="col s4 m6 l3 right-align uploadarea">
                        <i class="zmdi zmdi-cloud-upload"></i>&nbsp;&nbsp;<a class='dropdown-button btn' href='javascript:void(0)'><span>UPLOAD</span></a>
                     </div>
                  </div>
                  <div class="row subheader_i subheader_second">
                     <div class="col s12 m6 l8 tabarea">
                        <div class="fake-title-area divided-nav mobile-header">
                           <ul class="tabs">
                              <li class="tab col s3"><a class="active" href="#pages-suggested" data-toggle="tab" aria-expanded="false">Recent</a></li>
                              <li class="tab col s3"><a href="#pages-liked" data-toggle="tab" aria-expanded="false">Popular</a></li>
                              <li class="tab col s3"><a href="#pages-yours" data-toggle="tab" aria-expanded="false">Your</a></li>
                           </ul> 
                        </div>
                     </div>
                     <div class="col s12 m6 l4 right-align categoriesarea">
                        <a href="javascript:void(0)" class="dropdown-button more_btn" data-activates='categoriesarea_drp'>All categories <i class="mdi mdi-chevron-down"></i></a>
                        <ul id='categoriesarea_drp' class='dropdown-content custom_dropdown'>
                           <li><a href="javascript:void(0)">Animals</a></li>
                           <li><a href="javascript:void(0)">Celebrities</a></li>
                           <li><a href="javascript:void(0)">City & Architecture</a></li>
                           <li><a href="javascript:void(0)">Concert</a></li>
                           <li><a href="javascript:void(0)">Family</a></li>
                           <li><a href="javascript:void(0)">Fashion</a></li>
                           <li><a href="javascript:void(0)">Film</a></li>
                           <li><a href="javascript:void(0)">Fine Art</a></li>
                           <li><a href="javascript:void(0)">Food</a></li>
                           <li><a href="javascript:void(0)">Landscapes</a></li>
                           <li><a href="javascript:void(0)">Nature</a></li>
                           <li><a href="javascript:void(0)">People</a></li>
                           <li><a href="javascript:void(0)">Sport</a></li>
                           <li><a href="javascript:void(0)">Still Life</a></li>
                           <li><a href="javascript:void(0)">Transportation</a></li>
                           <li><a href="javascript:void(0)">Travel</a></li>
                           <li><a href="javascript:void(0)">Underwater</a></li>
                           <li><a href="javascript:void(0)">Urban</a></li>
                           <li><a href="javascript:void(0)">Wedding</a></li>
                        </ul>
                     </div>
                  </div>


                  <div class="tab-content view-holder grid-view" style="margin: 0;">
                     <div class="tab-pane fade active in main-pane" id="pages-suggested" style="padding-top: 0;">
                        <div class="pages-list generalbox-list all-list">
                           <div class="clear"></div>
                           <div class="row">
                              <div class="placesphotos-content subtab places-discussion-main bottom_tabs">
                                 <div class="cbox-desc gallery-content">
                                    <div class="gloader" style="display: none;"><img src="images/loading.gif" class="g-loading"></div>
                                    <div class="lgt-gallery-photo lgt-gallery-justified">
                                       <div data-src="images/wgallery1.jpg" class="allow-gallery">
                                            <img class="himg" src="images/wgallery1.jpg"/>  
                                          <div class="caption">
                                             <div class="pull-left">
                                                <span class="title">Big Bird</span> <br>
                                                <span class="attribution">By Adel Hasanat</span>
                                             </div>
                                             <div class="pull-right icons">
                                                <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span>
                                                </a>
                                                <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-src="images/wgallery2.jpg" class="allow-gallery">
                                            <img class="himg" src="images/wgallery2.jpg"/>  
                                          <div class="caption">
                                             <div class="pull-left">
                                                <span class="title">Big Bird</span> <br>
                                                <span class="attribution">By Adel Hasanat</span>
                                             </div>
                                             <div class="pull-right icons">
                                                <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                                                <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                          <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                            <img class="himg" src="images/wgallery3.jpg"/>  
                                          <div class="caption">
                                             <div class="pull-left">
                                                <span class="title">Big Bird</span> <br>
                                                <span class="attribution">By Adel Hasanat</span>
                                             </div>
                                             <div class="pull-right icons">
                                                <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                                                <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                      </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane dis-none" id="pages-liked" style="padding-top: 0;">
                        <div class="pages-list generalbox-list liked-list">
                           <!-- <div class="filters">
                              <div class="friends-search">
                                 <div class="fsearch-form">
                                    <input type="text" placeholder="Search for a page"/>
                                    <a href="javascript:void(0)"><i class="zmdi zmdi-search"></i></a>
                                 </div>
                              </div>
                           </div> -->
                           <div class="clear"></div>
                           <div class="row">
                              <div class="placesphotos-content subtab places-discussion-main bottom_tabs">
                                 <div class="cbox-desc gallery-content">
                                    <div class="gloader" style="display: none;"><img src="images/loading.gif" class="g-loading"></div>
                                    <div class="lgt-gallery-photo lgt-gallery-justified">
                                       <div data-src="images/wgallery1.jpg" class="allow-gallery">
                                            <img class="himg" src="images/wgallery1.jpg"/>  
                                          <div class="caption">
                                             <div class="pull-left">
                                                <span class="title">Big Bird</span> <br>
                                                <span class="attribution">By Adel Hasanat</span>
                                             </div>
                                             <div class="pull-right icons">
                                                <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span>
                                                </a>
                                                <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-src="images/wgallery2.jpg" class="allow-gallery">
                                            <img class="himg" src="images/wgallery2.jpg"/>  
                                          <div class="caption">
                                             <div class="pull-left">
                                                <span class="title">Big Bird</span> <br>
                                                <span class="attribution">By Adel Hasanat</span>
                                             </div>
                                             <div class="pull-right icons">
                                                <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                                                <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                          <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                            <img class="himg" src="images/wgallery3.jpg"/>  
                                          <div class="caption">
                                             <div class="pull-left">
                                                <span class="title">Big Bird</span> <br>
                                                <span class="attribution">By Adel Hasanat</span>
                                             </div>
                                             <div class="pull-right icons">
                                                <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                                                <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                      </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane pages-yours dis-none" id="pages-yours" style="padding-top: 0;">
                        <div class="pages-list generalbox-list admin-list">
                           <div class="row">
                              <div class="placesphotos-content subtab places-discussion-main bottom_tabs">
                                 <div class="cbox-desc gallery-content">
                                    <div class="gloader" style="display: none;"><img src="images/loading.gif" class="g-loading"></div>
                                    <div class="lgt-gallery-photo lgt-gallery-justified">
                                       <div data-src="images/wgallery1.jpg" class="allow-gallery">
                                            <img class="himg" src="images/wgallery1.jpg"/>  
                                          <div class="caption">
                                             <div class="pull-left">
                                                <span class="title">Big Bird</span> <br>
                                                <span class="attribution">By Adel Hasanat</span>
                                             </div>
                                             <div class="pull-right icons">
                                                <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span>
                                                </a>
                                                <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                       <div data-src="images/wgallery2.jpg" class="allow-gallery">
                                            <img class="himg" src="images/wgallery2.jpg"/>  
                                          <div class="caption">
                                             <div class="pull-left">
                                                <span class="title">Big Bird</span> <br>
                                                <span class="attribution">By Adel Hasanat</span>
                                             </div>
                                             <div class="pull-right icons">
                                                <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                                                <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                          <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                            <img class="himg" src="images/wgallery3.jpg"/>  
                                          <div class="caption">
                                             <div class="pull-left">
                                                <span class="title">Big Bird</span> <br>
                                                <span class="attribution">By Adel Hasanat</span>
                                             </div>
                                             <div class="pull-right icons">
                                                <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                                                <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                      </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>
<!--add page modal-->
<div id="add-item-popup" class="modal add-item-popup custom_md_modal collec_main_cus add_collection_new dropdownheight145">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>			 
            <h3>Create a page</h3>
            <a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="main-pcontent">
            <form class="add-item-form">
               <div class="frow frowfull">
                  <div class="crop-holder" id="image-cropper">
                     <div class="cropit-preview"></div>
                     <div class="main-img">
                        <img src="images/additem-collections.png" class="ui-corner-all"/>
                     </div>
                     <div class="main-img1">
                        <img id="imageid" draggable="false"/>
                     </div>
                     <div class="btnupload custom_up_load" id="upload_img_action">
                        <div class="fileUpload">
                           <i class="zmdi zmdi-camera"></i>
                           <input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
                        </div>
                     </div>
                     <a  href="javascript:void(0)" class="btn btn-save image_save_btn image_save dis-none">
                     <span class="zmdi zmdi-check"></span>
                     </a>
                     <a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash">
                     <i class="mdi mdi-close" ></i>
                     </a>
                  </div>
               </div>
               <div class="sidepad">
                  <div class="frow">
                     <input id="page_title" type="text" class="validate item_title" placeholder="Page title" />					
                  </div>
                  <div class="frow dropdown782">
                     <select id="pageCatDrop1" class="pageservices" data-fill="n" data-action="pageservices" data-selectore="pageservices">
                     <?php
                        $page = array("Bags/Luggage" => "Bags/Luggage", "Camera/Photo" => "Camera/Photo", "Cars" => "Cars", "Clothing" => "Clothing", "Entertainment" => "Entertainment", "Professional Services" => "Professional Services", "Sporting Goods" => "Sporting Goods", "Kitchen/Cooking" => "Kitchen/Cooking", "Concert Tour" => "Concert Tour", "Concert Venue" => "Concert Venue", "Food/Beverages" => "Food/Beverages", "Outdoor Gear" => "Outdoor Gear", "Tour Operator" => "Tour Operator", "Travel Agency" => "Travel Agency", "Travel Services" => "Travel Services", "Attractions/Things to Do" => "Attractions/Things to Do", "Event Planning/Event Services" => "Event Planning/Event Services", "Hotel" => "Hotel", "Landmark" => "Landmark", "Movie Theater" => "Movie Theater", "Museum/Art gallery" => "Museum/Art gallery", "Outdoor Gear/Sporting Goods" => "Outdoor Gear/Sporting Goods", "Public Places" => "Public Places", "Travel Site" => "Travel Site", "Travel Destination" => "Travel Destination", "Organization" => "Organization", "Website" => "Website");
                        foreach ($page as $s9032n) {
                          echo "<option value=".$s9032n.">$s9032n</option>";
                        }
                     ?>
                     </select>
                  </div>
                  <div class="frow">
                     <textarea id="Collection_tagline" class="materialize-textarea mb0 md_textarea item_tagline" placeholder="Short description of your page"></textarea>
                     <span class="char-limit">0/80</span>								
                  </div>
                  <div class="frow">
                     <textarea type="text" placeholder="Tell people more about the page" class="materialize-textarea md_textarea item_about"></textarea>
                  </div>
                  <div class="frow">
                     <input type="text" id="compose_mapmodalAction" class="validate item_title compose_mapmodalAction" placeholder="Bussines address 'City/Country'" data-query="all" onfocus="filderMapLocationModal(this)" autocomplete="off">
                  </div>
                  <div class="frow">
                     <input id="extweb" type="text" class="validate item_title" placeholder="List your external website, if you have one" />
                  </div>
                  <div class="frow">
                     <span class="icon-span"><input type="radio" id="agreeemailpage" name="verify-radio"></span>
                     <p>Veryfiy ownership by sending  a text message to following email</p>
                  </div>
                  <div class="frow">
                     <input type="text" placeholder="Your company email address">
                  </div>
                  <div class="frow">
                     <input type="checkbox" id="create_page" />
                     <label for="create_page">I verify that I am the official representative of this entity and have the right to act on behalf of my entity in the creation of this page.</label>						
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Create</a>
   </div>
</div>
<?php include('common/discard_popup.php'); ?>
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include("script.php"); ?>			
</body>
</html>