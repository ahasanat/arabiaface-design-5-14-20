
<div id="compose_camera" class="modal compose_tool_box post-popup custom_modal main_modal imgcrop-pop">
   <div class="modal-content  text-imgcrop">
      <div class="new-post active">
         <div class="clear"></div>
         <div class="scroll_div">
            <div class="npost-content">
               <div class="post-mcontent">
                  <div class="cropper cropper-wrapper">
                     <div class="crop" style="display:none;">
                        <div class="green-top desktop-view showon">Drag to crop</div>
                        <div class="js-cropping"></div>
                        <i class="js-cropper-result--btn zmdi zmdi-check upload-btn"></i>
                        <i class="mdi mdi-close img-cancel-btn" onclick="$('.js-cropper-result').show();$('.crop').hide();$('.image-upload').show();"></i>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>