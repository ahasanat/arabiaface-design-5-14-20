<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="images/favicon.ico">
      <title>Arabiaface</title>
      <link href="css/materialize.css" rel="stylesheet">
      <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/animate.css" rel="stylesheet"/>
      <link href="css/material-icons.css" rel="stylesheet">
      <link href="css/material-design-iconic-font.css" rel="stylesheet">
      <link href="css/tooltipster.bundle.min.css" rel="stylesheet">
      <link href="css/tooltipster-sideTip-borderless.min.css" rel="stylesheet">
      <link href="css/custom-plugin.css" rel="stylesheet">
      <link href="css/template.css" rel="stylesheet">
      <link href="css/custom-croppie.css" rel="stylesheet">
      <link href="css/master-responsive.css" rel="stylesheet">
      <link href="css/themes.css" rel="stylesheet">
      <link href="css/materialdesignicons.css" rel="stylesheet">
      <link href="css/datepicker.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" media="screen" href="css/all-ie-only.css"  />
      <script src="js/jquery.min.js"></script>
      <!--  <script src="js/materialize.js" type="text/javascript" charset="utf-8" ></script> -->
   </head>
   <body class="theme-color">
      <div class="home-page bodydup pageloader">
         <div id="loader-wrapper" class="home_loader">
            <div class="loader-logo">
               <div class="lds-css ng-scope">
                  <div class="lds-rolling lds-rolling100">
                     <div></div>
                  </div>
               </div>
            </div> 
            <!--<div id="loader"></div>-->
            <div class="loader-text">Please wait...</div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
         </div>
         <div class="home-wrapper">
         <header>
            <div class="home-container">
               <div class="mobile-menu topicon">
                  <a href="javascript:void(0)" class="mbl-menuicon"><i class="mdi mdi-menu"></i></a>
               </div>
               <div class="hlogo-holder">
                  <a class="home-logo" href="javascript:void(0)"><img src="images/arabiaface-logo-white.png"></a>                              
               </div>
               <ul class="home-menu">
                  <li><a href="index.php">Home</a></li>
                  <li><a href="collections.php">Collection</a></li>
                  <li><a href="channels.php">Channels</a></li>
                  <li><a href="who-is-around.php">People</a></li>
                  <li><a href="ad-manager.php"><i class="zmdi zmdi-plus-square pr-5"></i>Create Advert</a></li>
               </ul>
               <div class="head-right">
                  <div class="search-part">
                     <a class="homebtn" onclick="flipSectionTo('login');" href="javascript:void(0)">
                     <i class="mdi mdi-lock"></i>
                     <span>Login</span> 
                     </a>
                  </div>
                  <div class="signup-part">
                     <a class="homebtn" onclick="flipSectionTo('login');" href="javascript:void(0)">
                     <i class="mdi mdi-lock"></i>
                     <span>Login</span>
                     </a>
                  </div>
                  <div class="login-part">
                     <a class="homebtn" onclick="flipSectionTo('search');" href="javascript:void(0)">
                     <i class="mdi mdi-close"></i>
                     <span>Close</span>
                     </a>
                  </div>
                  <div class="forgot-part">
                     <a class="homebtn" onclick="flipSectionTo('login');" href="javascript:void(0)">
                     <i class="mdi mdi-lock"></i>
                     <span>Login</span>
                     </a>
                  </div>
               </div>
            </div>
         </header>
         <div class="sidemenu-holder m-hide">
            <div class="sidemenu">
               <a href="javascript:void(0)" class="closemenu waves-effect waves-theme"><i class="mdi mdi-close"></i>`</a>          
               <div class="side-user">                   
                  Check this out
               </div>
               <div class="sidemenu-ul">
                  <ul class="large-menuicons">
                     <li><a href="collections.php">Collection</a></li>
                     <li><a href="channels.php">Channels</a></li>
                     <li><a href="community-events.php">Events</a></li>
                     <li><a href="who-is-around.php">People</a></li>
                     <li><a href="ad-manager.php"><i class="zmdi zmdi-plus-square pr-5"></i>Create Advert</a></li>
                  </ul>
               </div>
            </div>
         </div>

         <div class="hcontent-holder banner-section">
            <span class="overlay"></span>
            <div class="home-content">
               <div class="search-part homel-part">
                  <div class="container">
                     <div class="search-box">
                        <div class="box-content">
                           <div class="login-notice">
                              <span class="success-note">Successfully Logged in!</span>
                              <span class="info-note">Fill in the mandatory fields.</span>
                              <span class="error-note">Please Enter Email address and Password</span>
                           </div>
                           <div class="bc-row home-search">
                              <div class="row">
                                 <!-- <h3 class="data-privacy white-text">Social Network for Friends and Family</h3> -->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="login-part homel-part login-sec">
                  <div class="hidden_header">
                    <div class="content_header">
                      <button class="close_span cancel_poup waves-effect" onclick="flipSectionTo('search');">
                          <i class="mdi mdi-close mdi-20px"></i>
                      </button>
                      <p class="modal_header_xs">Login</p>
                    </div>
                  </div>
                  <div class="container">
                     <div class="homebox login-box animated wow zoomIn" data-wow-duration="1200ms" data-wow-delay="500ms">
                        <div class="sociallink-area">                   
                           <a href="/arabiaface-code/frontend/web?r=site/auth&amp;authclient=facebook" class="fb-btn white-text"><span><i class="mdi mdi-facebook"></i></span>Connect with Facebook</a>
                        </div>
                        <div class="home-divider">
                           <span class="div-or">or</span>
                        </div>
                        <div class="box-content">
                           <div class="login-notice">
                              <span class="success-note">Successfully Logged in!</span>
                              <span class="info-note">Fill in the mandatory fields.</span>
                              <span class="error-note">Please Enter Email address and Password</span>
                           </div>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" placeholder="Email Address">
                                 </div>
                              </div>
                           </div>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="password" placeholder="Password">
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                        </div>
                        <div class="nextholder login-popup">
                           <a class="fp-link" href="javascript:void(0)" onclick="setForgotPassStep();flipSectionTo('forgot');">Forgot Password?</a>
                           <a href="javascript:void(0)" class="homebtn">Login</a>
                        </div>
                        <div class="btn-holder">
                           <p>Do not have an account?
                              <a onclick="flipSectionTo('signup');" href="javascript:void(0)" class="white-text">Sign Up</a>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="signup-part homes-part">
            <div class="hidden_header">
              <div class="content_header">
                <button class="close_span cancel_poup waves-effect" onclick="flipSectionTo('search');">
                 <i class="mdi mdi-close mdi-20px"></i>
                </button>
                <p class="modal_header_xs">Sign Up</p>
              </div>
            </div>
                  <div class="container">

                     <div class="homebox signup-box" id="create-account">
                        <div class="box-content">
                           <div class="signup-notice">
                              <span class="success-note">Successfully Logged in!</span>
                              <span class="info-note">Fill in the mandatory fields.</span>
                              <span class="error-note">Please Enter Email address and Password</span>
                           </div>
                           <h5>Create Account</h5>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" placeholder="First Name">
                                 </div>
                              </div>
                           </div>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" placeholder="Last Name">
                                 </div>
                              </div>
                           </div>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" placeholder="Email Address">
                                 </div>
                              </div>
                           </div>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="password" placeholder="Password">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="nextholder">
                           <a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="profile-setting" onclick="signupNavigation(this)">Next</a>                        
                        </div>
                        <div class="btn-holder">
                           <p>Have an account?
                              <a onclick="flipSectionTo('login');" href="javascript:void(0)" class="white-text">Login</a>
                           </p>
                        </div>
                     </div>
                     <div class="homebox signup-box profile-setting-new" id="profile-setting">
                        <div class="profile-settingblock1">
                        <div class="box-content">
                           <div class="signup-notice">
                              <span class="success-note">Successfully Logged in!</span>
                              <span class="info-note">Fill in the mandatory fields.</span>
                              <span class="error-note">Please Enter Email address and Password</span>
                           </div>
                           <h5>Profile Setting</h5>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" data-query="all" id="icon_telephonenew" class="validate" onfocus="filderMapLocationModalSignup(this)" autocomplete='off' placeholder="City">
                                 </div>
                              </div>
                           </div>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" placeholder="Country">
                                 </div>
                              </div>
                           </div>
                           <div class="bc-row">
                              <div class="bc-componentnew">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <select>
                                       <option value="Male">Male</option>
                                       <option value="Female">Female</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" data-toggle="datepicker" class="datepickerinputtemp" data-query="all" placeholder="Birthdate" readonly/>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="nextholder">
                           <a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="upload-photo" onclick="signupNavigation(this)">Next</a>
                        </div>
                        <div class="btn-holder">
                           <p>Have an account?
                              <a onclick="flipSectionTo('login');" href="javascript:void(0)" class="white-text">Login</a>
                           </p>
                        </div>
                        </div>
                        <div class="profile-settingblock2">
                           <div class="content_header">
                              <button class="close_span waves-effect">
                                 <i class="mdi mdi-close mdi-20px material_close resetdatepicker"></i>
                              </button>
                              <p class="selected_photo_text">Select Date</p>
                              <a href="javascript:void(0)" class="done_btn action_btn closedatepicker">Done</a>
                           </div> 
                           <div class="modal-content">
                              <div id="datepickerBlocktemp"></div>
                           </div>
                        </div>

                        <div class="profile-settingblock3 inlinemapmodalbox">
                           <div class="map-model-content">  
                             <div style="display:none">
                               <div class="map_header_container">
                                 <div class="search_container">
                                   <button class="close_span waves-effect"> 
                                     <i class="mdi mdi-close mdi-20px material_close"></i>
                                   </button>
                                   <div class="near_by_place">
                                     <input placeholder="Search nearby places.." id="nearby_place_new" type="text" class="validate nearby_place_new specialinput">
                                   </div>
                                   <span class="search_span search_map_new" id="map_search_new">
                                     <i class="zmdi zmdi-search material_close mdi-18px"></i>
                                   </span>
                                 </div>
                               </div>
                               <div class="map_frame">
                                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d238133.1523816246!2d72.68221020433099!3d21.15914250210564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04e59411d1563%3A0xfe4558290938b042!2sSurat%2C+Gujarat!5e0!3m2!1sen!2sin!4v1507141795136" style="width:100%;max-width:100%;height:200px" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                               </div>
                               <div class="map_container"></div>
                             </div>
                             <div id="pac_container_new"  class="map_header_container">
                               <div class="search_container">
                                 <div class="map_remove">
                                   <button class="close_span waves-effect">
                                     <i class="mdi mdi-close mdi-20px close_search_box"></i>
                                   </button>
                                 </div>
                                 <button class="back_arrow"> 
                                   <i class="zmdi zmdi-arrow-left mdi-20px"></i>
                                 </button>
                                 <input id="pac-input_new" class="controls specialinput" type="text" placeholder="Search Your Location">
                                 <span class="search_span search_map_new" id="map_search_new">
                                   <i class="zmdi zmdi-search material_close mdi-18px"></i>
                                 </span>
                                 <span class="empty_input empty_close">
                                   <i class="mdi mdi-close mdi-20px"></i>
                                 </span>
                               </div>
                             </div>
                             <div id="map_new" class="map_div"></div>
                             <div class="map_container"> </div>
                           </div>
                        </div>
                     </div>
                     <div class="homebox signup-box upload-photo" id="upload-photo">
                        <div class="box-content">
                           <div class="signup-notice">
                              <span class="success-note">Successfully Logged in!</span>
                              <span class="info-note">Fill in the mandatory fields.</span>
                              <span class="error-note">Please Enter Email address and Password</span>
                           </div>
                           <div class="upload-header">
                           </div>
                           <div class="home-cropper">
                              <div class="cropper cropper-wrapper">
                                 <div class="image-upload">
                                    <label for="file-input">
                                    <i class="zmdi zmdi-camera-bw"></i>
                                    </label>
                                    <input id="file-input" type="file" class="js-cropper-upload" value="Select" onclick="$('.js-cropper-result').hide();$('.crop').show();$('.image-upload').hide();$('.cropper-nae').hide();$('.text-hide-show').hide();"/>
                                 </div>
                                 <div class="js-cropper-result">
                                    <img class="circle" src="images/demo-profile.jpg" />
                                 </div>
                                 <div class="crop dis-none">
                                    <div class="grag-title">Drag to crop</div>
                                    <div class="js-cropping"></div>
                                    <i class="js-cropper-result--btn zmdi zmdi-check upload-btn" onclick="$('.cropper-nae').show();"></i>
                                    <i class="js-cropper-result--btn zmdi zmdi-check upload-btn" onclick="$('.cropper-nae').show();$('.text-hide-show').show();"></i>
                                    <i class="mdi mdi-close img-cancel-btn" onclick="$('.js-cropper-result').show();$('.crop').hide();$('.image-upload').show();$('.cropper-nae').show();$('.text-hide-show').show();"></i>
                                 </div>
                                 <h1 class="cropper-nae">Adel Hasanat</h1>
                              </div>
                              <p class="note red-danger text-hide-show">
                                 Your photo needs to be 200x200 with gif or jpeg format
                              </p>
                           </div>
                        </div>
                        <div class="nextholder">
                           <a href="javascript:void(0)" class="homebtn su-skipbtn left" data-class="security-check" onclick="signupNavigation(this)">Skip</a>
                           <a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="security-check" onclick="signupNavigation(this)">Next</a>
                        </div>
                        <div class="btn-holder">
                           <p>Have an account?
                              <a onclick="flipSectionTo('login');" href="javascript:void(0)" class="white-text">Login</a>
                           </p>
                        </div>
                     </div>
                     <div class="homebox signup-box" id="security-check">
                        <div class="box-content">
                           <div class="signup-notice">
                              <span class="success-note">Successfully Logged in!</span>
                              <span class="info-note">Fill in the mandatory fields.</span>
                              <span class="error-note">Please Enter Email address and Password</span>
                           </div>
                           <h5>Security Check</h5>
                           <div class="security-box">
                              <div class="row">
                                 <p>To guard against automated systems and robots, please checked the box below</p>
                              </div>
                              <div class="row center">
                                 <input type="checkbox" class="filled-in robot-chk new-robot-chec" id="filled-in-box" />
                                 <label for="filled-in-box">I am not robot</label>
                              </div>
                              <div class="row center pt-25">
                                 <input type="checkbox" class="filled-in robot-chk agree-chk" id="filled-in-box2" />
                                 <label for="filled-in-box2" class="home-f-n">Agreed to the terms and conditions</label>
                              </div>
                           </div>
                        </div>
                        <div class="nextholder">
                           <a href="javascript:void(0)" class="homebtn su-nextbtn" data-class="confirm-email" onclick="signupNavigation(this)">Next</a>
                        </div>
                        <div class="btn-holder">
                           <p>Have an account?
                              <a onclick="flipSectionTo('login');" href="javascript:void(0)" class="white-text">Login</a>
                           </p>
                        </div>
                     </div>
                     <div class="homebox signup-box" id="confirm-email">
                        <div class="box-content">
                           <div class="signup-notice">
                              <span class="success-note">Successfully Logged in!</span>
                              <span class="info-note">Fill in the mandatory fields.</span>
                              <span class="error-note">Please Enter Email address and Password</span>
                           </div>
                           <h5>You’re almost done</h5>
                           <div class="text-center fullwidth">
                              <img src="images/confirm-msg.png" class="confirm-img"/>  
                           </div>
                           <h6 class="white-text">Please confirm your email to have full access to your account</h6>
                        </div>
                        <div class="nextholder">
                           <a class="homebtn autow" href="javascript:void(0)">Confirm Email</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="forgot-part homes-part">
                  <div class="container">
                     <div class="homebox forgot-box" id="fp-step-1">
                        <div class="box-content">
                           <div class="fphome-notice">
                              <span class="success-note">Successfully Logged in!</span>
                              <span class="info-note">Fill in the mandatory fields.</span>
                              <span class="error-note">Please Enter Email address and Password</span>
                           </div>
                           <h5>Change Your Password</h5>
                           <div class="fp-box">
                              <p class="text-center">Let's find your account</p>
                              <div class="bc-row mt25">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" placeholder="Email address or alternate email">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="nextholder">
                           <a href="javascript:void(0)" class="homebtn" data-class="fp-step-2" onclick="forgotPassNavigation(this)">Next</a>
                        </div>
                     </div>
                     <div class="homebox forgot-box" id="fp-step-2">
                        <div class="box-content">
                           <h5>We've sent a link to change your password</h5>
                           <div class="fp-box">
                              <p>check your email and follow the link to quickly reset your password</p>
                           </div>
                        </div>
                        <div class="nextholder">
                           <a href="javascript:void(0)" class="homebtn autow checke-link">Check Email</a>
                           <a href="javascript:void(0)" class="homebtn" data-class="fp-step-3" onclick="forgotPassNavigation(this)">Next</a>
                        </div>
                     </div>
                     <div class="homebox forgot-box" id="fp-step-3">
                        <div class="box-content">
                           <div class="fphome-notice">
                              <span class="success-note">Successfully Logged in!</span>
                              <span class="info-note">Fill in the mandatory fields.</span>
                              <span class="error-note">Please Enter Email address and Password</span>
                           </div>
                           <h5>Choose New Password</h5>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" placeholder="Type your new password">
                                 </div>
                              </div>
                           </div>
                           <div class="bc-row">
                              <div class="bc-component">
                                 <div class="sliding-middle-out anim-area underlined">
                                    <input type="text" placeholder="Confirm your new password">
                                 </div>
                              </div>
                           </div>
                           <p class="note">Passwords are case sensitive, must be at least 6 characters</p>
                        </div>
                        <div class="nextholder">
                           <a href="javascript:void(0)" class="homebtn" data-class="fp-step-4" onclick="forgotPassNavigation(this)">Continue</a>
                        </div>
                     </div>
                     <div class="homebox forgot-box" id="fp-step-4">
                        <div class="box-content">
                           <h5>Your password has been reset</h5>
                           <div class="fp-box">
                              <p class="text-center">Now you can login with your new password!</p>
                           </div>
                        </div>
                        <div class="nextholder">
                           <a href="javascript:void(0)" class="homebtn" onclick="flipSectionTo('login');">Log in</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="banner-footer">
               <div class="home-container">
                  <div class="row">
                     <div class="col-lg-12 f-left">
                        <div class="user-count">
                           <p>
                              Social Networking for the Arab Community !!
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- <div class="pattern"></div> -->
         </div>

         <!-- row2 start -->
         <div class="home-section socially-connected">
            <div class="container">
               <div class="section home-row2">
                  <div class="home-title">
                     <h4>Socially Connected!</h4>
                     <p>Social Media and network for friends and family</p>
                  </div>
                  <!--   Icon Section-->
                  <div class="socials-section socially-connected">
                     <div class="row">
                        <div class="col s12 m4 wow animated zoomIn" data-wow-delay="500ms" data-wow-duration="1200ms">
                           <div class="icon-block">
                              <img src="images/social-discover.png" class="center-block" alt="img 1">
                              <div class="descholder green-box">
                                 <h3 class="font-25">MESSAGE</h3>
                                 <h6>WHERE TO GO</h6>
                                 <p>Stay in tounch and chat or call you friends and family</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 wow animated zoomIn" data-wow-delay="500ms" data-wow-duration="1200ms">
                           <div class="icon-block">
                              <img src="images/social-share.png" class="center-block" alt="img 1">
                              <div class="descholder green-box">
                                 <h3 class="font-25">SHARE</h3>
                                 <h6>YOUR EXPERIENCES</h6>
                                 <p>Share your moments and photos with your friends and acquaintances</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 hidden-sm wow animated zoomIn" data-wow-delay="500ms" data-wow-duration="1200ms">
                           <div class="icon-block">
                              <img src="images/social-meet.png" class="center-block" alt="img 1">
                              <div class="descholder green-box">
                                 <h3 class="font-25">MEET</h3>
                                 <h6>LIKE-MINDED PEOPLE</h6>
                                 <p>Make friends with people that share values and interests</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- row2 end -->
         <div class="hcontent-holder home-section state-section row-icon">
            <div class="container">
               <div class="state-area">
                  <div class="row center">
                     <div class="col m3 s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="0ms">
                        <div class="iconholder">
                           <i class="zmdi zmdi-accounts"></i>
                        </div>
                        <div class="descholder">
                           <h3>62</h3>
                           <p>People</p>
                        </div>
                     </div>
                     <div class="m3 col s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="3ms">
                        <div class="iconholder">
                           <i class="mdi mdi-clipboard"></i>
                        </div>
                        <div class="descholder">
                           <h3>758</h3>
                           <p>Posts</p>
                        </div>
                     </div>
                     <div class="m3 col s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="6ms">
                        <div class="iconholder">
                           <i class="mdi mdi-file-image"></i>
                        </div>
                        <div class="descholder">
                           <h3>357</h3>
                           <p>Photos</p>
                        </div>
                     </div>
                     <div class="m3 col s6 wow bounceInUp" data-wow-duration="0.7s" data-wow-delay="9ms">
                        <div class="iconholder">
                           <i class="zmdi zmdi-check zmdi-hc-lg"></i>
                        </div>
                        <div class="descholder">
                           <h3>29</h3>
                           <p>Events</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="hcontent-holder home-section gray-section popular-places">
            <div class="container mt-20">
                <div class="home-title general-page ">
                    <h4>POPULAR COLLECTIONS</h4>
                    <p class="mt-15">With a world full of fascinating ideas, suggesting  the most popular collections to follow</p>
                    <div class="row mt-20">
                        <div class="tab-content view-holder grid-view">
                            <div class="tab-pane fade main-pane active in" id="collections-suggested">
                                <div class="generalbox-list">
                                    <div class="row">
                                        <div class="col s6 m4 l3 gridBox127 specialramani collectionbox collectionbox_5bc1025790d5b61963b3bf9f">
                                            <div class="card hoverable collectionCard animated fadeInUp collection-title">
                                                <a href="javascript:void(0);" class="general-box collection-red" onclick="collectionFollows(event,'5bc1025790d5b61963b3bf9f',this, true)">
                                                    <div class="photo-holder">
                                                        <img src="images/additem-collections.png">
                                                    </div>
                                                    <div class="content-holder">
                                                        <h4>Testing</h4>
                                                        <div class="icon-line">
                                                            <span>yesno</span>
                                                        </div>
                                                        <div class="userinfo">
                                                            <img src="images/Male.jpg"/>
                                                        </div>
                                                        <div class="username">
                                                            <span>Adel Hasanat</span>
                                                        </div>
                                                        <div class="action-btns">                 
                                                            <span class="noClick" onclick="collectionFollows(event,'5bc1025790d5b61963b3bf9f',this, false)">Follow</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col s6 m4 l3 gridBox127 collectionbox collectionbox_5ae5a972ea01e359171a7c23">
                                            <div class="card hoverable collectionCard animated fadeInUp collection-title">
                                                <a href="javascript:void(0);" class="general-box collection-red" onclick="collectionFollows(event,'5ae5a972ea01e359171a7c23',this, true)">
                                                    <div class="photo-holder">
                                                        <img src="images/additem-collections.png">
                                                    </div>
                                                    <div class="content-holder">
                                                        <h4>Jordan Food</h4>
                                                        <div class="icon-line">
                                                            <span>uuu</span>
                                                        </div>
                                                        <div class="userinfo">
                                                            <img src="images/Male.jpg"/>
                                                        </div>
                                                        <div class="username">
                                                            <span>Adel Hasanat</span>
                                                        </div>
                                                        <div class="action-btns">                 
                                                            <span class="noClick" onclick="collectionFollows(event,'5ae5a972ea01e359171a7c23',this, false)">Follow</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col s6 m4 l3 gridBox127 collectionbox collectionbox_5a9feb52ea01e3142e237953">
                                            <div class="card hoverable collectionCard animated fadeInUp collection-title">
                                                <a href="javascript:void(0);" class="general-box collection-red" onclick="collectionFollows(event,'5a9feb52ea01e3142e237953',this, true)">
                                                    <div class="photo-holder">
                                                        <img src="images/additem-collections.png">
                                                    </div>
                                                    <div class="content-holder">
                                                        <h4>London Travel</h4>
                                                        <div class="icon-line">
                                                            <span>ade</span>
                                                        </div>
                                                        <div class="userinfo">
                                                            <img src="images/Male.jpg"/>
                                                        </div>
                                                        <div class="username">
                                                            <span>Adel Hasanat</span>
                                                        </div>
                                                        <div class="action-btns">                 
                                                            <span class="noClick" onclick="collectionFollows(event,'5a9feb52ea01e3142e237953',this, false)">Follow</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col s6 m4 l3 gridBox127 collectionbox collectionbox_5a9d3df7ea01e3987d23769e">
                                            <div class="card hoverable collectionCard animated fadeInUp collection-title">
                                                <a href="javascript:void(0);" class="general-box " onclick="collectionFollows(event,'5a9d3df7ea01e3987d23769e',this, true)">
                                                    <div class="photo-holder">
                                                        <img src="images/additem-collections.png">
                                                    </div>
                                                    <div class="content-holder">
                                                        <h4>Test</h4>
                                                        <div class="icon-line">
                                                            <span>test</span>
                                                        </div>
                                                        <div class="userinfo">
                                                            <img src="images/Male.jpg"/>
                                                        </div>
                                                        <div class="username">
                                                            <span>Shweta Agrawal</span>
                                                        </div>
                                                        <div class="action-btns">                 
                                                            <span class="noClick" onclick="collectionFollows(event,'5a9d3df7ea01e3987d23769e',this, false)">Follow</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
         </div>


         <div class="hcontent-holder home-section promo-section promo1-section dark-section">
            <div class="container">
               <div class="promo-area">
                  <div class="row">
                     <div class="col m4 s12 hidden-sm wow bounceInLeft"></div>
                  </div>
               </div>
            </div>
         </div>
         <div class="hcontent-holder home-section">
            <div class="container">
               <div class="collection-area">
                  <div class="row">
                     <div class="col m6 s12 wow slideInLeft event-img">
                        <div class="imgholder">
                           <img src="images/travel-story.jpg" alt="travel event"/>                 
                        </div>
                     </div>
                     <div class="col m6 s12">
                        <div class="descholder travel-story">
                           <h3 class="animated wow zoomIn" data-wow-duration="1200ms" data-wow-delay="500ms">COLLECTIONS</h3>
                           <p class="animated wow fadeInLeft" data-wow-duration="1200ms" data-wow-delay="500ms">A collection can focus on a set of posts on a particular topic, providing an easy way for you to organize all your experiences in one place. Each collection can be shared publicly, privately, or with your friend. People follow your stories to receive updates on your added posts.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="hcontent-holder home-section meetpeople-section dark-section">
            <div class="container">
               <div class="meetpeople-area expert">
                  <div class="row">
                     <div class="col m6 s12">
                        <div class="home-title">
                           <h3>Top Rankers</h3>
                        </div>
                     </div>
                     <div class="col m6 s12 wow bounceInUp">
                        <div class="meetpoeple-box">
                           <div class="people">
                              <div class="boxleft">
                                 <div class="imgholder">
                                    <img src="images/recent-2.png"/>
                                 </div>
                                 <h4>Alex</h4>
                                 <p>New York</p>
                                 <a href="javascript:void(0)"><i class="mdi mdi-arrow-right"></i> View Wall</a>
                              </div>
                              <div class="boxright">
                                 <div class="imgholder">
                                    <img src="images/recent-3.png"/>
                                 </div>
                                 <h4>Anesthesia</h4>
                                 <p>London</p>
                                 <a href="javascript:void(0)"><i class="mdi mdi-arrow-right"></i> View Wall</a>
                              </div>
                              <div class="boxleft">
                                 <div class="imgholder">
                                    <img src="images/recent-2.png"/>
                                 </div>
                                 <h4>John</h4>
                                 <p>Germany</p>
                                 <a href="javascript:void(0)"><i class="mdi mdi-arrow-right"></i> View Wall</a>
                              </div>
                              <div class="boxright">
                                 <div class="imgholder">
                                    <img src="images/recent-3.png"/>
                                 </div>
                                 <h4>Maria</h4>
                                 <p>Australia</p>
                                 <a href="javascript:void(0)"><i class="mdi mdi-arrow-right"></i> View Wall</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="hcontent-holder home-section gray-section tours-page tours">
             <div class="container mt-10">
                 <div class="home-title general-page ">
                     <h4>Channels</h4>
                     <p class="mt-15">Channels are topics open for discussion assigned by system Admin.</p>
                     <div class="tab-content view-holder grid-view">
                         <div class="tab-pane fade main-pane active in" id="channels-suggested">
                             <div class="generalbox-list channels-list">
                                 <div class="row">
                                     <div class="col s6 m4 l3 gridBox127 channelbox channelbox_5adc68c9ea01e37c791a7c1a">
                                         <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="javascript:void(0);" onclick="channelSubscribe(event, '5adc68c9ea01e37c791a7c1a', this, true, 1)" class="general-box">
                                                 <div class="photo-holder">
                                                     <img src="images/additem-channels.png">
                                                 </div>
                                                 <div class="content-holder collection-title">
                                                     <h4>Jordan Food</h4>
                                                     <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                     </div>
                                                     <div class="countinfo">
                                                         1 </div>
                                                     <div class="posts-line">
                                                         1 posts </div>
                                                     <div class="action-btns">
                                                         <span class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</span>
                                                     </div>
                                                 </div>
                                             </a>
                                         </div>
                                     </div>
                                     <div class="col s6 m4 l3 gridBox127 channelbox channelbox_5adc6823ea01e3b2791a7bd2">
                                         <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="javascript:void(0);" onclick="channelSubscribe(event, '5adc6823ea01e3b2791a7bd2', this, true, 2)" class="general-box">
                                                 <div class="photo-holder">
                                                     <img src="images/additem-channels.png">
                                                 </div>
                                                 <div class="content-holder collection-title">
                                                     <h4>Jordan Travel</h4>
                                                     <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                     </div>
                                                     <div class="countinfo">
                                                         2 </div>
                                                     <div class="posts-line">
                                                         1 posts </div>
                                                     <div class="action-btns">
                                                         <span class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</span>
                                                     </div>
                                                 </div>
                                             </a>
                                         </div>
                                     </div>
                                     <div class="col s6 m4 l3 gridBox127 channelbox channelbox_59d4baffea01e3264cec0213">
                                         <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="javascript:void(0);" onclick="channelSubscribe(event, '59d4baffea01e3264cec0213', this, true, 3)" class="general-box">
                                                 <div class="photo-holder">
                                                     <img src="images/additem-channels.png">
                                                 </div>
                                                 <div class="content-holder collection-title">
                                                     <h4>Arabiaface channel</h4>
                                                     <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                     </div>
                                                     <div class="countinfo">
                                                         3 </div>
                                                     <div class="posts-line">
                                                         3 posts </div>
                                                     <div class="action-btns">
                                                         <span class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</span>
                                                     </div>
                                                 </div>
                                             </a>
                                         </div>
                                     </div>
                                     <div class="col s6 m4 l3 gridBox127 channelbox channelbox_59d4bae1ea01e3293cec026b">
                                         <div class="card hoverable channelCard animated fadeInUp">
                                             <a href="javascript:void(0);" onclick="channelSubscribe(event, '59d4bae1ea01e3293cec026b', this, true, 4)" class="general-box">
                                                 <div class="photo-holder">
                                                     <img src="images/additem-channels.png">
                                                 </div>
                                                 <div class="content-holder collection-title">
                                                     <h4>Travel a way</h4>
                                                     <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                     </div>
                                                     <div class="countinfo">
                                                         4 </div>
                                                     <div class="posts-line">
                                                         1 posts </div>
                                                     <div class="action-btns">
                                                         <span class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</span>
                                                     </div>
                                                 </div>
                                             </a>
                                         </div>
                                     </div>

                                 </div>
                             </div>
                         </div>
                     </div>

                 </div>
             </div>
         </div>
         <div class="petra-siq">
            <div class="hcontent-holder home-section info-section info2-section china-petra">
               <div class="container">
                  <div class="info-area">
                     <div class="row">
                        <div class="col m6 s12 right petra">
                           <div class="home-title">
                              <h4>ENCRYPTION</h4>
                           </div>
                           <p class="para-petra">
                              Your privacy and protecting you data is consider by our team is the most important issue during the design. Before we launch the finial copy of the website, member names, email, phone number, contact, message will be encrypted.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="hcontent-holder home-section gray-section news-section">
               <div class="container">
                  <div class="feedback-area">
                     <div class="row">
                        <div class="col m4 s12 wow slideInUp">
                           <div class="feedbackbox">
                              <div class="imgholder">
                                 <img src="images/feedback-1.png"/>
                              </div>
                              <div class="descholder">
                                 <p>Travel is all about new experiences. No matter where you're going, Touristlink gives you opportunity to get a real feel of the culture. Meet up with a local for a coffee or beer,</p>
                              </div>
                           </div>
                        </div>
                        <div class="col m4 s12 wow slideInUp">
                           <div class="feedbackbox">
                              <div class="imgholder">
                                 <img src="images/feedback-2.PNG"/>
                              </div>
                              <div class="descholder">
                                 <p>Travel is all about new experiences. No matter where you're going, Touristlink gives you opportunity to get a real feel of the culture. Meet up with a local for a coffee or beer,</p>
                              </div>
                           </div>
                        </div>
                        <div class="col m4 s12 wow slideInUp hidden-xs">
                           <div class="feedbackbox">
                              <div class="imgholder">
                                 <img src="images/feedback-3.PNG"/>
                              </div>
                              <div class="descholder">
                                 <p>Travel is all about new experiences. No matter where you're going, Touristlink gives you opportunity to get a real feel of the culture. Meet up with a local for a coffee or beer,</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <footer class="footertag">
               <div class="footer-cols">
                  <div class="container">
                     <div class="row">
                        <div class="col m6 s12">
                           <h5>Email</h5>
                           <i class="mdi mdi-email white-text"></i>
                           <p>
                              General: <a href="mailto:office@yoursite.com" title="">office@Arabiaface.com</a>
                              <br />
                              Support: <a href="mailto:support@example.com" title="">support@Arabiaface.com</a>
                           </p>
                        </div>
                        <div class="col m6 s12">
                           <h5 class="">Follow</h5>
                           <i class="mdi mdi-facebook"></i> &nbsp; &nbsp;
                           <i class="mdi mdi-twitter"></i>
                           <p>
                              <a href="www.facebook.com" target="_blank" title="">Find us on Facebook</a>
                              <br />
                              <a href="www.twitter.com" target="_blank" title="">Get us on Twitter</a>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="copyright">
                  <div class="container">
                     <p>&copy; Arabiaface 2019. All rights reserved.</p>
                  </div>
               </div>
            </footer>
         </div>
      </div>
            
      <!-- popups --> 
      <?php include('common/datepicker.php'); ?>

      <div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1 map_modalUniq" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 8%;">
         <?php include('common/map_modal.php'); ?>
      </div>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
      <script>
         var wow = new WOW();
         wow.init();
      </script>
      <?php include("script.php"); ?>     
   </body>
</html>


<script>
    if($(window).width() < 767)
    {
      $(".search-part .homebtn").click(function(){
         $("body").addClass("hide_header");
      });
      $(".homes-part .close_span, .homel-part .close_span").click(function(){
         $("body").removeClass("hide_header");
      });      
    } else { 
         $("body").removeClass("hide_header");
    }
   var cropper = $('.cropper');

   if (cropper.length) {
     $.each(cropper, function(i, val) {
       var uploadCrop;

       var readFile = function(input) {
         if (input.files && input.files[i]) {
           var reader = new FileReader();

           reader.onload = function(e) {
             uploadCrop.croppie('bind', {
               url: e.target.result
             });
           };

           reader.readAsDataURL(input.files[i]);
         } else {
           alert("Sorry - you're browser doesn't support the FileReader API");
         }
       };

       uploadCrop = $('.js-cropping').croppie({ // TODO: fix so its selects right element
         viewport: {
           width: 200,
           height: 200
         },
         boundary: {
           width: 375,
           height: 360
         },
         enableOrientation: true
       });

       $('.js-cropper-upload').on('change', function() {
         $('.crop').show(); // TODO: fix so its selects right element
         readFile(this);
       });
       
       $('.js-cropper-rotate--btn').on('click', function(ev) {
         uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
       });

       $('.js-cropper-result--btn').on('click', function(ev) {
         uploadCrop.croppie('result', 'canvas').then(function(resp) {
           popupResult({
             src: resp
           });
         });
       });

       var popupResult = function(result) {
         var html;

         if (result.html) {
           html = result.html;
         }

         if (result.src) {
           html = '<img src="' + result.src + '" />';
         }
         $('.js-cropper-result').show();  
         $('.js-cropper-result').html(html); // TODO: fix so its selects right element
         $('.crop').hide();
         $('.image-upload').show();
       };
     });
   }
</script>