
<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div> 
<div class="container page_container pages_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="main-content with-lmenu pages-page main-page grid-view general-page">
         <div class="combined-column comsepcolumn">
            <div class="content-box nbg">
               <div class="cbox-desc md_card_tab">
                  <div class="fake-title-area divided-nav mobile-header">
                     <ul class="tabs">
                        <li class="tab col s3"><a class="active" href="#pages-suggested" data-toggle="tab" aria-expanded="false">Suggested</a></li>
                        <li class="tab col s3"><a href="#pages-liked" data-toggle="tab" aria-expanded="false">Liked</a></li>
                        <li class="tab col s3"><a href="#pages-yours" data-toggle="tab" aria-expanded="false">Yours</a></li>
                     </ul> 
                  </div>
                  <div class="tab-content view-holder grid-view">
                     <div class="tab-pane fade active in main-pane" id="pages-suggested">
                        <div class="pages-list generalbox-list all-list">
                           <div class="clear"></div>
                           <div class="row">
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">										
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane dis-none" id="pages-liked">
                        <div class="pages-list generalbox-list liked-list">
                           <div class="filters">
                              <div class="friends-search">
                                 <div class="fsearch-form">
                                    <input type="text" placeholder="Search for a page"/>
                                    <a href="javascript:void(0)"><i class="zmdi zmdi-search"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="row">
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane pages-yours dis-none" id="pages-yours">
                        <div class="pages-list generalbox-list admin-list">
                           <div class="row">
                              <div class="col s6 m4 l3 gridBox127 add-cbox">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <div class="general-box">
                                       <a href="javascript:void(0)" class="add-page add-general" onclick="openAddItemModal()">
                                       <span class="icont">+</span>
                                       Add new page
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable pageCard animated fadeInUp">
                                    <a href="javascript:void(0);" onclick="doPageLike(event,'page1',this)" class="page-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/businessbanner.jpg">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Dallas Travel &amp; Tourism</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/demo-business.jpg"/>
                                          </div>
                                          <div class="username">
                                             <span>2320 Likes</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             2320 Likes
                                          </div>
                                          <div class="action-btns">														
                                             <span class="noClick" onclick="doPageLike(event,'page1',this)">Like</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>
<!--add page modal-->
<div id="add-item-popup" class="modal add-item-popup custom_md_modal collec_main_cus add_collection_new dropdownheight145">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>			 
            <h3>Create a page</h3>
            <a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="main-pcontent">
            <form class="add-item-form">
               <div class="frow frowfull">
                  <div class="crop-holder" id="image-cropper">
                     <div class="cropit-preview"></div>
                     <div class="main-img">
                        <img src="images/additem-collections.png" class="ui-corner-all"/>
                     </div>
                     <div class="main-img1">
                        <img id="imageid" draggable="false"/>
                     </div>
                     <div class="btnupload custom_up_load" id="upload_img_action">
                        <div class="fileUpload">
                           <i class="zmdi zmdi-camera"></i>
                           <input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
                        </div>
                     </div>
                     <a  href="javascript:void(0)" class="btn btn-save image_save_btn image_save dis-none">
                     <span class="zmdi zmdi-check"></span>
                     </a>
                     <a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash">
                     <i class="mdi mdi-close" ></i>
                     </a>
                  </div>
               </div>
               <div class="sidepad">
                  <div class="frow">
                     <input id="page_title" type="text" class="validate item_title" placeholder="Page title" />					
                  </div>
                  <div class="frow dropdown782">
                     <select id="pageCatDrop1" class="pageservices" data-fill="n" data-action="pageservices" data-selectore="pageservices">
                     <?php
                        $page = array("Bags/Luggage" => "Bags/Luggage", "Camera/Photo" => "Camera/Photo", "Cars" => "Cars", "Clothing" => "Clothing", "Entertainment" => "Entertainment", "Professional Services" => "Professional Services", "Sporting Goods" => "Sporting Goods", "Kitchen/Cooking" => "Kitchen/Cooking", "Concert Tour" => "Concert Tour", "Concert Venue" => "Concert Venue", "Food/Beverages" => "Food/Beverages", "Outdoor Gear" => "Outdoor Gear", "Tour Operator" => "Tour Operator", "Travel Agency" => "Travel Agency", "Travel Services" => "Travel Services", "Attractions/Things to Do" => "Attractions/Things to Do", "Event Planning/Event Services" => "Event Planning/Event Services", "Hotel" => "Hotel", "Landmark" => "Landmark", "Movie Theater" => "Movie Theater", "Museum/Art gallery" => "Museum/Art gallery", "Outdoor Gear/Sporting Goods" => "Outdoor Gear/Sporting Goods", "Public Places" => "Public Places", "Travel Site" => "Travel Site", "Travel Destination" => "Travel Destination", "Organization" => "Organization", "Website" => "Website");
                        foreach ($page as $s9032n) {
                          echo "<option value=".$s9032n.">$s9032n</option>";
                        }
                     ?>
                     </select>
                  </div>
                  <div class="frow">
                     <textarea id="Collection_tagline" class="materialize-textarea mb0 md_textarea item_tagline" placeholder="Short description of your page"></textarea>
                     <span class="char-limit">0/80</span>								
                  </div>
                  <div class="frow">
                     <textarea type="text" placeholder="Tell people more about the page" class="materialize-textarea md_textarea item_about"></textarea>
                  </div>
                  <div class="frow">
                     <input type="text" id="compose_mapmodalAction" class="validate item_title compose_mapmodalAction" placeholder="Bussines address 'City/Country'" data-query="all" onfocus="filderMapLocationModal(this)" autocomplete="off">
                  </div>
                  <div class="frow">
                     <input id="extweb" type="text" class="validate item_title" placeholder="List your external website, if you have one" />
                  </div>
                  <div class="frow">
                     <span class="icon-span"><input type="radio" id="agreeemailpage" name="verify-radio"></span>
                     <p>Veryfiy ownership by sending  a text message to following email</p>
                  </div>
                  <div class="frow">
                     <input type="text" placeholder="Your company email address">
                  </div>
                  <div class="frow">
                     <input type="checkbox" id="create_page" />
                     <label for="create_page">I verify that I am the official representative of this entity and have the right to act on behalf of my entity in the creation of this page.</label>						
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Create</a>
   </div>
</div>
<?php include('common/discard_popup.php'); ?>
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include("script.php"); ?>			
</body>
</html>