<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include("common/allcss.php"); ?>   
   </head>
   <body class="theme-color">
      <div class="page-wrapper menutransheader-wrapper menuhideicons-wrapper">
         <div class="header-section">
            <?php include("header1.php"); ?>
         </div>
         <div class="floating-icon">
            <div class="scrollup-btnbox anim-side btnbox scrollup-float">
               <div class="scrollup-button float-icon">
                  <span class="icon-holder ispan">
                  <i class="mdi mdi-arrow-up-bold-circle"></i>
                  </span>
               </div>
            </div>
         </div>
         <div class="clear"></div>
         <div class="container page_container fulltab">
            <?php include("common/leftmenu.php"); ?>
            <div class="fixed-layout ipad-mfix">
               <div class="main-content with-lmenu general-page generaldetails-page channels-page channeldetails-page main-page">
                  <div class="combined-column">
                     <div class="content-box">
                        <div class="cbox-title nborder">
                           <i class="mdi mdi-view-grid-large"></i>
                           Channels
                           <a href="channels.php" class="backbtn"><i class="mdi mdi-menu-left"></i> Back to channels</a>
                        </div>
                        <div class="cbox-desc">
                           <div class="tab-content view-holder">
                              <div class="general-details">
                                 <div class="gdetails-summery">
                                    <div class="main-info">
                                       <div class="imgholder">
                                          <img src="images/channel-img-2.png">
                                          <div class="back-link">
                                             <a href="channels.php" class="waves-effect waves-theme"><i class="mdi mdi-arrow-left"></i></a>
                                          </div>
                                          <div class="action-links item_detail_dropdown">
                                             <a href="javascript:void(0)" class="orglink share-it waves-effect waves-theme">
                                             <i class="zmdi zmdi-mail-reply zmdi-hc-lg zmdi-hc-flip-horizontal"></i>
                                             </a>
                                             <div class="settings-icon">
                                                <a class="dropdown-button waves-theme waves-effect" href="javascript:void(0)" data-activates="detail_setting">
                                                <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                </a>
                                                <ul id="detail_setting" class="dropdown-content custom_dropdown">
                                                   <li>
                                                      <a href="javascript:void(0)" onclick="manageMembers()">Manage Members</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" onclick="openMembersList()">Channel subscribers</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" onclick="PreferenceModel()">Preferences</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" onclick="generateDiscard('dis_unsub')">Unsubscribe channel</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="content-holder dateholder">
                                          <div class="countinfo">
                                             2
                                          </div>
                                       </div>
                                       <div class="content-holder gdetails-moreinfo expandable-holder">
                                          <a href="javascript:void(0)" class="expandable-link invertsign" onclick="mng_expandable(this)"><i class="mdi mdi-chevron-down"></i></a>
                                          <div class="expandable-area">
                                             <div class="username"><span>Roman Range</span></div>
                                             <h4>Beaches To Travel</h4>
                                             <div class="username tagline">
                                                <span>7 awesome beaches across the world</span>
                                             </div>
                                             <div class="icon-line">
                                                <span class="fcount">1,840 subscribers</span>
                                                <span>-</span>
                                                <span>452 posts</span>
                                                <span>-</span>
                                                <span>public</span>
                                             </div>
                                             <div class="action-btns">								
                                                <a class="noClick" onclick="channelSubscribe(event,'channel1',this)">Subscribe</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="sideboxes">
                                       <?php include('common/recently_joined.php'); ?>
                                       <?php include('common/travads.php'); ?>
                                    </div>
                                 </div>
                                 <div class="post-column">
                                    <?php include('common/new_post.php'); ?>  
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php include('common/chat.php'); ?>		
                  <div class="new-post-mobile clear">
                     <a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
                  </div>
               </div>
            </div>
         </div>
         <?php include("common/footer.php"); ?>
      </div>	
      <?php  include('common/preference_popup.php'); ?>
      <div id="channel_subscriber" class="modal item_members">
         <div class="modal_header">
            <button class="close_btn custom_modal_close_btn close_modal">
            <i class="mdi mdi-close mdi-20px"></i>
            </button>
            <h3>Channel Subscribers</h3>
         </div>
         <div class="custom_modal_content modal_content">
            <div class="follower-container">
               <ul>
                  <li>
                     <div class="follwer_profile">
                        <img class="circle" src="images/demo-profile.jpg" />
                     </div>
                     <div class="follwer_name">
                        <span>User Name</span>
                     </div>
                  </li>
                  <li>
                     <div class="follwer_profile">
                        <img class="circle" src="images/demo-profile.jpg" />
                     </div>
                     <div class="follwer_name">
                        <span>User Name</span>
                     </div>
                  </li>
                  <li>
                     <div class="follwer_profile">
                        <img class="circle" src="images/demo-profile.jpg" />
                     </div>
                     <div class="follwer_name">
                        <span>User Name</span>
                     </div>
                  </li>
                  <li>
                     <div class="follwer_profile">
                        <img class="circle" src="images/demo-profile.jpg" />
                     </div>
                     <div class="follwer_name">
                        <span>User Name</span>
                     </div>
                  </li>
                  <li>
                     <div class="follwer_profile">
                        <img class="circle" src="images/demo-profile.jpg" />
                     </div>
                     <div class="follwer_name">
                        <span>User Name</span>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <!--map modal-->
      <div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
         <?php include('common/map_modal.php'); ?>
      </div>
      <?php include('common/uploadphoto_popup.php'); ?>
      <?php include('common/addperson_popup.php'); ?>
      <?php include('common/compose_post_popup.php'); ?>

      <?php include('common/comment_popup.php'); ?>

      <?php include('common/postopen_popup.php'); ?>

      <?php include('common/share_popup.php'); ?>

      <?php include('common/editpost_popup.php'); ?>

      <div id="managemembers-popup" class="modal manage-modal managemembers-popup">
         <div class="modal_header">
            <button class="close_btn custom_modal_close_btn close_modal waves-effect">
            <i class="mdi mdi-close mdi-20px"></i>
            </button>
            <h3>Manage Members</h3>
         </div>
         <div class="custom_modal_content modal_content">
            <div class="main-pcontent spadding">
               <ul class="tabs">
                  <li class="tab"><a href="#member-request" data-toggle="tab" aria-expanded="false">Member Requested</a></li>
                  <li class="tab"><a href="#member-all" class="active" data-toggle="tab" aria-expanded="true">All Members</a></li>
               </ul>
               <div class="tab-content">
                  <div id="member-request" class="tab-pane fade">
                     <!--<h5>Member requests</h5>-->
                     <ul class="manage-members">
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm">Accept</a>
                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm">Reject</a>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm">Accept</a>
                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm">Reject</a>
                                 </div>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
                  <div id="member-all" class="tab-pane fade active">
                     <!--<h5>All Members</h5>-->
                     <ul class="manage-members">
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Admin</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings1">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings1" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Option</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Member</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings2">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings2" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Remove member</a></li>
                                          <li><a href="javascript:void(0)">Ban member</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Member</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings3">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings3" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Remove member</a></li>
                                          <li><a href="javascript:void(0)">Ban member</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Admin</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings1">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings1" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Option</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Member</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings2">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings2" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Remove member</a></li>
                                          <li><a href="javascript:void(0)">Ban member</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Member</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings3">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings3" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Remove member</a></li>
                                          <li><a href="javascript:void(0)">Ban member</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Admin</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings1">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings1" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Option</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Member</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings2">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings2" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Remove member</a></li>
                                          <li><a href="javascript:void(0)">Ban member</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Member</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings3">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings3" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Remove member</a></li>
                                          <li><a href="javascript:void(0)">Ban member</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Admin</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings1">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings1" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Option</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Member</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings2">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings2" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Remove member</a></li>
                                          <li><a href="javascript:void(0)">Ban member</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="member-li">
                              <div class="imgholder"><img class="circle" src="images/demo-profile.jpg"/></div>
                              <div class="descholder">
                                 <span class="head6">User Name</span>
                                 <div class="settings">
                                    <span class="status">Member</span>
                                    <div class="right">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="mm_settings3">
                                       <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                       </a>
                                       <ul id="mm_settings3" class="dropdown-content custom_dropdown">
                                          <li><a href="javascript:void(0)">Remove member</a></li>
                                          <li><a href="javascript:void(0)">Ban member</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include('common/discard_popup.php'); ?>
      <?php include("script.php"); ?>	
   </body>
</html>