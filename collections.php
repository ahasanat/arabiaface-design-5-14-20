<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container collection_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="main-content with-lmenu collections-page main-page grid-view general-page">
         <div class="combined-column comsepcolumn">
            <div class="content-box nbg">
               <div class="cbox-desc md_card_tab">
                  <div class="fake-title-area divided-nav mobile-header">
                     <ul class="tabs">
                        <li class="tab col s3">
                           <a class="active" href="#collections-suggested" data-toggle="tab" aria-expanded="true">Suggested</a>
                        </li>
                        <li class="tab col s3">
                           <a href="#collections-following" data-toggle="tab" aria-expanded="false">Following</a> 
                        </li>
                        <li class="tab col s3">
                           <a href="#collections-yours" data-toggle="tab" aria-expanded="false">Yours</a>
                        </li>
                     </ul>
                  </div>
                  <div class="tab-content view-holder grid-view">
                     <div class="tab-pane fade main-pane active" id="collections-suggested">
                        <div class="generalbox-list">
                           <div class="row">
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-collections.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Tokyo Street Fashion</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-1.png"/>
                                          </div>
                                          <div class="username">
                                             <span>In love with Japan</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">														
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="collectionFollow(event,'collection1',this)">Follow</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-2.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Renewable energy</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-1.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Greg Batmarx</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="collectionFollow(event,'collection2',this)">Follow</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-3.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Black-white illustrations to H. L.</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-3.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Henry Lion Oldie</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="collectionFollow(event,'collection3',this)">Follow</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-4.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>B&W Photography</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-1.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Rui Luis</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="collectionFollow(event,'collection4',this)">Follow</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-5.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Light in Motion</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-5.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Alex Lapidus</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="collectionFollow(event,'collection5',this)">Follow</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-6.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>India</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-6.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Suzanne Bell</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="collectionFollow(event,'collection6',this)">Follow</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-7.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>My Feathered Friends</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-2.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Chwee Hock Low</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="collectionFollow(event,'collection7',this)">Follow</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane dis-none" id="collections-following">
                        <div class="generalbox-list">
                           <div class="row">
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-collections.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Tokyo Street Fashion</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-1.png"/>
                                          </div>
                                          <div class="username">
                                             <span>In love with Japan</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">														
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="collectionFollow(event,'collection1',this)">Following..</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-3.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Black-white illustrations to H. L.</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-3.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Henry Lion Oldie</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="collectionFollow(event,'collection3',this)">Following..</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-6.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>India</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-6.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Suzanne Bell</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick" onclick="collectionFollow(event,'collection6',this)">Following..</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane collections-yours general-yours dis-none" id="collections-yours">
                        <div class="generalbox-list">
                           <div class="row">
                              <div class="col s6 m4 l3 add-cbox gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <div class="general-box">
                                       <a href="javascript:void(0)" class="add-collection add-general" onclick="openAddItemModal()">
                                       <span class="icont">+</span>
                                       Create collection
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-2.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Renewable energy</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-1.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Greg Batmarx</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick">Owner</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-5.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Light in Motion</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-5.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Alex Lapidus</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick">Owner</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-6.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>India</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-6.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Suzanne Bell</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick">Owner</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable collectionCard animated fadeInUp">
                                    <a href="collections-detail.php" class="general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/collection-img-7.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>My Feathered Friends</h4>
                                          <div class="icon-line">
                                             <span>Tagline goes here</span>
                                          </div>
                                          <div class="userinfo">
                                             <img src="images/chat-2.png"/>
                                          </div>
                                          <div class="username">
                                             <span>Chwee Hock Low</span>
                                          </div>
                                          <div class="icon-line">
                                             <i class="zmdi zmdi-check"></i>
                                             Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                          </div>
                                          <div class="icon-line subtext">														
                                             5 Attending
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">120 Followers</span>
                                             <span class="noClick">Owner</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>

<?php include('common/upload_img_box.php'); ?>
<?php include('common/privacymodal.php'); ?>

<!--add collection modal-->
<div id="add_collection_modal" class="modal add-item-popup custom_md_modal collec_main_cus add_collection_new">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>			
            <h3>Create a collection</h3>
            <a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="main-pcontent">
            <form class="add-item-form">
               <div class="frow frowfull">
                  <div class="crop-holder" id="image-cropper">
                     <div class="cropit-preview"></div>
                     <div class="main-img">
                        <img src="images/additem-collections.png" class="ui-corner-all"/>
                     </div>
                     <div class="main-img1">
                        <img id="imageid" draggable="false"/>
                     </div>
                     <div class="btnupload custom_up_load" id="upload_img_action">
                        <div class="fileUpload">
                           <i class="zmdi zmdi-camera zmdi-hc-lg"></i>
                           <input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
                        </div>
                     </div>
                     <a  href="javascript:void(0)" class="btn btn-save image_save_btn image_save dis-none">
                     <i class="zmdi zmdi-check"></i>
                     </a>
                     <a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash">
                     <i class="mdi mdi-close"></i>
                     </a>
                  </div>
               </div>
               <div class="sidepad">
                  <div class="frow">
                     <input id="Collection_title" type="text" class="validate item_title" placeholder="Collection title" />
                  </div>
                  <div class="frow">
                     <textarea id="Collection_tagline" class="materialize-textarea mb0 md_textarea item_tagline" placeholder="Collection tagline" data-length="80"></textarea>
                  </div>
                  <div class="frow security-area">
                     <label>Visible To:</label>
                     <div class="right">
                        <a class="dropdown-button collectioncreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="collectioncreateprivacylabel">
                           <span> Public </span>
                           <i class="zmdi zmdi-caret-down"></i>
                        </a>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Create</a>
   </div>
</div>
<?php include('common/custom_modal.php'); ?>
<?php include('common/discard_popup.php'); ?>
<?php include("script.php"); ?>
</body>
</html>